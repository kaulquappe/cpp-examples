#include <iostream>
using namespace std;

/**
 * In this version of a singleton pattern the single instance of a class is static and
 * constructed on the stack at the beginning of runtime as a static member.
 * The static method getInstance() always returns a reference to the same instance.
 */
class SingletonStackReference 
{
    private:
        SingletonStackReference(){;}
        static SingletonStackReference instance_;
        SingletonStackReference(SingletonStackReference const&);
        void operator=(SingletonStackReference const&);

    public:
        void print()
        {
            cout << '\t' << hex << &SingletonStackReference::instance_ << endl;
        }
        
        static SingletonStackReference& getInstance()
        {
            return SingletonStackReference::instance_;
        }
};
SingletonStackReference SingletonStackReference::instance_;

/**
 * In this version of a singleton pattern the single instance of a class is static
 * and constructed on the heap at the beginning of runtime as a static member.
 * The static method getInstance() always returns a pointer to the same instance.
 */
class SingletonHeapNotLazy 
{
    private:
        SingletonHeapNotLazy(){;}
        static SingletonHeapNotLazy* instance_;
        SingletonHeapNotLazy(SingletonHeapNotLazy const&);
        void operator=(SingletonHeapNotLazy const&);

    public:
        void print()
        {
            cout << '\t' << hex << SingletonHeapNotLazy::instance_ << endl;
        }
        
        static SingletonHeapNotLazy* getInstance()
        {
            return SingletonHeapNotLazy::instance_;
        }
};
SingletonHeapNotLazy* SingletonHeapNotLazy::instance_= new SingletonHeapNotLazy();


/**
 * In this version of a singleton pattern a static pointer to the single instance of a class
 * is initialized at the first call of the method getInstance() (lazy initialization).
 * The static method getInstance() always returns a pointer to the same object. 
 */
class SingletonHeapLazy 
{
    private:
        SingletonHeapLazy(){;}
        static SingletonHeapLazy* instance_;
        SingletonHeapLazy(SingletonHeapLazy const&);
        void operator=(SingletonHeapLazy const&);

    public:
        void print()
        {
            cout << '\t' << hex << SingletonHeapLazy::instance_ << endl;
        }
        
        static SingletonHeapLazy* getInstance()
        {
            if (SingletonHeapLazy::instance_ == 0)
                SingletonHeapLazy::instance_ = new SingletonHeapLazy();
            return SingletonHeapLazy::instance_;
        }
};
SingletonHeapLazy* SingletonHeapLazy::instance_= 0;

/**
 * In this version of a singleton pattern a static object of the single instance of a class
 * is initialized at the first call of the method getInstance() (lazy initialization).
 * The static method getInstance() always returns a reference to the same object. 
 * @see https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
 */
class SingletonStackReferenceLazy
{
    public:
        static SingletonStackReferenceLazy& getInstance()
        {
            static SingletonStackReferenceLazy instance_;   // Guaranteed to be destroyed.
                                                            // Instantiated on first use.
            return instance_;
        }
    private:
        SingletonStackReferenceLazy() {}                    // Constructor? (the {} brackets) are needed here.

        // C++ 03
        // ========
        // Don't forget to declare these two. You want to make sure they
        // are unacceptable otherwise you may accidentally get copies of
        // your singleton appearing.
        //~ SingletonStackReferenceLazy(SingletonStackReferenceLazy const&);        // Don't Implement
        //~ void operator=(SingletonStackReferenceLazy const&);                     // Don't implement

        // C++ 11
        // =======
        // We can use the better technique of deleting the methods
        // we don't want.
    public:
        SingletonStackReferenceLazy(SingletonStackReferenceLazy const&) = delete;
        void operator=(SingletonStackReferenceLazy const&) = delete;

        // Note: Scott Meyers mentions in his Effective Modern
        //       C++ book, that deleted functions should generally
        //       be public as it results in better error messages
        //       due to the compilers behavior to check accessibility
        //       before deleted status
        void print()
        {
            cout << '\t' << hex << &SingletonStackReferenceLazy::getInstance() << endl;
        }
};

int main()
{
    cout << "\nExample prints out two different calls of the print method for each of" << endl << 
    "four different implementation of the singleton pattern and shows that" << endl <<
    "the called object is at the same memory location." << endl;

    cout << "\n\n1. In this version of a singleton pattern the single instance of a class is static and" << endl <<
    "   constructed on the stack at the beginning of runtime as a static member." << endl <<
    "   The static method getInstance() always returns a reference to the same instance." << endl;
    SingletonStackReference& srClass = SingletonStackReference::getInstance();
    srClass.print();
    SingletonStackReference& sr2Class = SingletonStackReference::getInstance();
    sr2Class.print();

    cout << "\n2. In this version of a singleton pattern the single instance of a class is static" << endl <<
    "   and constructed on the heap at the beginning of runtime as a static member." << endl <<
    "   The static method getInstance() always returns a pointer to the same instance." << endl;
    SingletonHeapNotLazy* shnlClass = SingletonHeapNotLazy::getInstance();
    shnlClass->print();
    SingletonHeapNotLazy* shnl2Class = SingletonHeapNotLazy::getInstance();
    shnl2Class->print();

    cout << "\n3. In this version of a singleton pattern a static pointer to the single instance of a class" << endl <<
    "   is initialized at the first call of the method getInstance() (lazy initialization)." << endl <<
    "   The static method getInstance() always returns a pointer to the same object." << endl;
    SingletonHeapLazy* shlClass = SingletonHeapLazy::getInstance();
    shlClass->print();
    SingletonHeapLazy* shl2Class = SingletonHeapLazy::getInstance();
    shl2Class->print();

    cout << "\n4. In this version of a singleton pattern a static object of the single instance of a class" << endl <<
    "   is initialized at the first call of the method getInstance() (lazy initialization)." << endl <<
    "   The static method getInstance() always returns a reference to the same object."  << endl;
    SingletonStackReferenceLazy& srlClass = SingletonStackReferenceLazy::getInstance();
    srlClass.print();
    SingletonStackReferenceLazy& srl2Class = SingletonStackReferenceLazy::getInstance();
    srl2Class.print();

    cout << endl;
    return 0;
}
