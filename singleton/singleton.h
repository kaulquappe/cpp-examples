#include <iostream>


using namespace std;


class Singleton
{
  public:
	// returns a static reference to an object of class Singleton
    static Singleton& Instance(string name="")
    {
        static Singleton instance(name);
        return instance;
    }

	const string& getName() { return name_; }
	
  protected:
	// Prevent construction
    Singleton(string name) : name_(name) 
    { 
		cerr << "Singleton(" << "\"" << name_ << "\")" << endl; 
	}
	// Prevent unwanted destruction
    ~Singleton() 
    { 
		cerr << "~Singleton(" << "\"" << name_ << "\")" << endl;
	}
    // Prevent construction by copying
    Singleton(const Singleton&);
    // Prevent assignment
    Singleton& operator=(const Singleton&);
 
  private:
    string name_;
};

