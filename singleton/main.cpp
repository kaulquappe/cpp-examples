#include <iostream>
#include "singleton.h"

using namespace std;

int main(void)
{
	cerr << "main(): start" << endl;	
	Singleton& myFirstSingleton = Singleton::Instance("FirstInstance");
	cerr << "myFirstSingleton::" << myFirstSingleton.getName() << endl;
	// only the first and one and only instance will be returned !
	Singleton& mySecondSingleton = Singleton::Instance("SecondInstance");
	cerr << "mySecondSingleton::" << mySecondSingleton.getName() << endl;
	// only the first and one and only instance will be returned !
	Singleton& myThirdSingleton = Singleton::Instance();
	cerr << "myThirdSingleton::" << myThirdSingleton.getName() << endl;

	cerr << "main(): end" << endl;		
	return 0;
}
