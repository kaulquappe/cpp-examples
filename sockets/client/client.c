#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

const int BUFFERLEN = 256;

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int clientId, nrBytes;
    struct sockaddr_in serverAddr;
    struct hostent *server;
    char buffer[BUFFERLEN];

    if (argc < 3)
    {
        fprintf(stderr,"Usage: %s hostname port\n", argv[0]);
        exit(1);
    }

    // socket call
    clientId = socket(AF_INET, SOCK_STREAM, 0);
    if (clientId < 0) error( "Error: could not open client socket!\n");

    // get server 
    server = gethostbyname(argv[1]);
    if (server == NULL)
    {
        fprintf(stderr, "Error: no such host: %s\n", argv[1]);
        exit(1);
    }
    
    // connect to server
    bzero( &serverAddr, sizeof(serverAddr) );
    serverAddr.sin_family = AF_INET;
    bcopy( server->h_addr_list[0], &serverAddr.sin_addr.s_addr, server->h_length);
    serverAddr.sin_port = htons(atoi(argv[2]));
    if ( connect( clientId, (struct sockaddr*) &serverAddr, sizeof(serverAddr) ) < 0 )
        error("Error: could not connect to server!\n");

    // write to server
    printf("Please enter the message: ");
    bzero(buffer, BUFFERLEN);
    fgets(buffer, BUFFERLEN-1, stdin);
    nrBytes = write(clientId, buffer, strlen(buffer));
    if (nrBytes < 0) error("Error: could not write to server!\n");

    // read from Server
    bzero(buffer,BUFFERLEN);
    nrBytes = read(clientId, buffer, BUFFERLEN-1);
    if (nrBytes < 0) error("Error: could not read from server!\n");
    printf("%s\n",buffer);

    close(clientId);
    exit(0);
}
