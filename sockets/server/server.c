/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

const int BUFFLEN=256;

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int serverId;
    int clientId;
    int portNr;
    int nrBytes;
    socklen_t clientLen;
    char buffer[BUFFLEN];
    struct sockaddr_in serverAddr;
    struct sockaddr_in clientAddr;
    char* message = "I got your message!\n";

    if (argc < 2)
    {
        fprintf(stderr,"ERROR: no port provided!\n");
        exit(1);
    }
    
    // socket() call
    serverId = socket(AF_INET, SOCK_STREAM, 0);
    if (serverId < 0) error("ERROR: could not open socket!\n");

    // bind() call
    bzero( &serverAddr, sizeof(serverAddr));
    portNr = atoi(argv[1]);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(portNr);
    if (bind(serverId, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
    {
        error("ERROR: could not bind on ip address!\n");
    }

    // listen() call
    if( listen(serverId,5) < 0 )
    {
        error("ERROR: listen() call failed!\n");
    }

    // accept() call
    clientLen = sizeof(clientAddr);
    clientId = accept(serverId, (struct sockaddr *) &clientAddr, &clientLen);
    if (clientId < 0) error("ERROR: could not accept client!\n");

    // read from client
    bzero(buffer, sizeof(BUFFLEN));
    nrBytes = read(clientId, buffer, BUFFLEN-1);
    if (nrBytes < 0) error("ERROR: could not read from client socket!\n");
    printf("\nHere is the message: %s\n", buffer);

    // write to client
    nrBytes = write(clientId, message, strlen(message));
    if (nrBytes < 0) error("ERROR: could not write to client socket!\n");

    // close sockets
    close(clientId);
    close(serverId);
    
    exit(0);
}
