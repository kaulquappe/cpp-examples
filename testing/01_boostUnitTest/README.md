# BOOST UNIT TEST EXAMPLE



## Compilation
---

```
> g++ class2Test.hpp test.cpp -lboost_unit_test_framework -o test.exe
```

## Execution
---
```
> ./text.exe
> ./test.exe --help
> ./test.exe --help=log_level
> ./test.exe --help=report_format
> ./test.exe --help=report_level
> ./test.exe --report_level=detailed --report_format=HRF
> ./test.exe --report_level=detailed --report_format=XML
> ./test.exe --report_level=confirm --report_format=HRF
> ./test.exe --log_level=all --report_level=confirm --report_format=HRF
```

## Output
---
```
> make && ./test.exe --log_level=all --report_level=short  --report_format=HRF
g++ -c -Wall -Wextra -Wpedantic -std=c++11 -g  -o test.o test.cpp
g++  -o test.exe test.o  -lboost_unit_test_framework 
Running 3 test cases...
Entering test module "boostUnitTest"
test.cpp(37): Entering test case "TEST_NUMBERS"
test.cpp(44): info: check myTestClass.add(ia, ia) == 4 has passed
test.cpp(45): error: in "TEST_NUMBERS": 2 + 2 > 4 is not true!
test.cpp(46): error: in "TEST_NUMBERS": check myTestClass.add(fa, fb) == 4.002 has failed [4.00100088 != 4.0019999999999998]
test.cpp(47): error: in "TEST_NUMBERS": check myTestClass.add(fa, fb) == 4.0019 has failed [4.00100088 != 4.0019]
test.cpp(48): info: check myTestClass.add(fa, fb) == 4.0019 has passed
test.cpp(37): Leaving test case "TEST_NUMBERS"; testing time: 260us
test.cpp(51): Entering test case "TEST_STRINGS"
test.cpp(56): info: check myTestClass.hello() == "Hello you !" has passed
test.cpp(57): info: check myTestClass.hii() == "Hi Dude !" has passed
test.cpp(58): error: in "TEST_STRINGS": check myTestClass.hii() == "Who are u ?" has failed [Hi Dude ! != Who are u ?]
test.cpp(51): Leaving test case "TEST_STRINGS"; testing time: 188us
test.cpp(61): Entering test case "TEST_EXCEPTIONS"
test.cpp(65): info: check 'exception "std::exception" raised as expected' has passed
test.cpp(66): info: check 'exception "std::runtime_error" raised as expected' has passed
test.cpp(67): fatal error: in "TEST_EXCEPTIONS": unexpected exception thrown by myTestClass.throwException()
test.cpp(61): Leaving test case "TEST_EXCEPTIONS"; testing time: 277us
Leaving test module "boostUnitTest"; testing time: 772us

Test module "boostUnitTest" has failed with:
  3 test cases out of 3 failed
  1 test case out of 3 aborted
  6 assertions out of 11 passed
  5 assertions out of 11 failed

```

```
> ./test.exe 
g++  -o test.exe test.o  -lboost_unit_test_framework 
Running 3 test cases...
test.cpp(45): error: in "TEST_NUMBERS": 2 + 2 > 4 is not true!
test.cpp(46): error: in "TEST_NUMBERS": check myTestClass.add(fa, fb) == 4.002 has failed [4.00100088 != 4.0019999999999998]
test.cpp(47): error: in "TEST_NUMBERS": check myTestClass.add(fa, fb) == 4.0019 has failed [4.00100088 != 4.0019]
test.cpp(58): error: in "TEST_STRINGS": check myTestClass.hii() == "Who are u ?" has failed [Hi Dude ! != Who are u ?]
test.cpp(67): fatal error: in "TEST_EXCEPTIONS": unexpected exception thrown by myTestClass.throwException()

*** 5 failures are detected in the test module "boostUnitTest"

```

## References
---
* https://www.boost.org/doc/libs/1_68_0/libs/test/doc/html/index.html
* https://stackoverflow.com/questions/2906095/boost-test-looking-for-a-working-non-trivial-test-suite-example-tutorial
* http://neyasystems.com/an-engineers-guide-to-unit-testing-cmake-and-boost-unit-tests/

## Author:
---
* kaulquappe (https://bitbucket.org/kaulquappe/)
