#ifndef __CLASS2TESTHPP__
#define __CLASS2TESTHPP__


#include <iostream>
#include <string>
#include <exception>


using namespace std;

class class2Test
{
public:
    class2Test() {}
    ~class2Test() {}

    int add( const int a, const int b ) const { return a+b; }
    float add( float const a, const float b ) const { return a+b; }
    const string hello() { return string("Hello you !"); }
    const char* hii() { return "Hi Dude !"; }
    void throwException() { throw std::runtime_error("Shit happens ..."); }
};

#endif
