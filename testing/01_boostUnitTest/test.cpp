////////////////////////////////////////////////////////////////////////////////
//
// See: 
//          https://www.boost.org/doc/libs/1_68_0/libs/test/doc/html/index.html
//
// Compile with: 
//          g++ class2Test.hpp test.cpp  -lboost_unit_test_framework -o test.exe
//
// Run executable like in these examples: 
//          ./text.exe
//          ./test.exe --help
//          ./test.exe --help=log_level
//          ./test.exe --help=report_format
//          ./test.exe --help=report_level
//          ./test.exe --report_level=detailed --report_format=HRF
//          ./test.exe --report_level=detailed --report_format=XML
//          ./test.exe --report_level=confirm --report_format=HRF
//          ./test.exe --log_level=all --report_level=confirm --report_format=HRF
//
////////////////////////////////////////////////////////////////////////////////

// Link dynamically to boost
#define BOOST_TEST_DYN_LINK

//Define our module name (prints at testing)
#define BOOST_TEST_MODULE "boostUnitTest"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>

// Include our class to be tested
#include "class2Test.hpp"


// ------------- Tests Follow --------------

BOOST_AUTO_TEST_CASE( TEST_NUMBERS )
{
    class2Test myTestClass;
    const int ia=2;
    const float fa=2.000001;
    const float fb=2.001;

    BOOST_CHECK_EQUAL(myTestClass.add(ia, ia), 4);
    BOOST_TEST(myTestClass.add(ia, ia) > 4, ia << " + " << ia << " > 4 is not true!");
    BOOST_CHECK_EQUAL(myTestClass.add(fa, fb), 4.002);
    BOOST_TEST(myTestClass.add(fa, fb) == 4.0019);
    BOOST_TEST(myTestClass.add(fa, fb) == 4.0019, boost::test_tools::tolerance(0.001));
}

BOOST_AUTO_TEST_CASE( TEST_STRINGS )
{
    class2Test myTestClass;

    // compare c-string with std::string
    BOOST_TEST(myTestClass.hello() == "Hello you !");
    BOOST_TEST(myTestClass.hii() == "Hi Dude !");
    BOOST_TEST(myTestClass.hii() == "Who are u ?");
}

BOOST_AUTO_TEST_CASE( TEST_EXCEPTIONS )
{
    class2Test myTestClass;

    BOOST_CHECK_THROW( myTestClass.throwException(), std::exception );
    BOOST_CHECK_THROW( myTestClass.throwException(), std::runtime_error );
    BOOST_REQUIRE_NO_THROW( myTestClass.throwException() );
}
