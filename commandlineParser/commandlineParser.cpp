/**
 * 
 * Small example to show how to use boost.program_options. 
 * @see https://theboostcpplibraries.com/boost.program_options#ex.program_options_01 
 *
 * 
 * Examples:
 * 
 * > ./commandlineParser.exe 
 * Pi: 3.14
 * 
 * > ./commandlineParser.exe -h
 * Options:
 *  -h [ --help ]         Help screen
 *  --pi arg (=3.1400001) Pi
 *  --age arg             Age
 *
 * > ./commandlineParser.exe --age 13
 * On age: 13
 * Age: 13
 * 
 */

#include <boost/program_options.hpp>
#include <iostream>

using namespace boost::program_options;

void on_age(int age)
{
  std::cout << "On age: " << age << '\n';
}

int main(int argc, const char *argv[])
{
  try
  {
    options_description desc{"Options"};
    desc.add_options()
      ("help,h", "This help message.")
      ("pi", value<float>()->default_value(3.14f), "Pi")
      ("age", value<int>()->notifier(on_age), "Age");

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count("help"))
      std::cout << desc << '\n';
    else if (vm.count("age"))
      std::cout << "Age: " << vm["age"].as<int>() << '\n';
    else if (vm.count("pi"))
      std::cout << "Pi: " << vm["pi"].as<float>() << '\n';
  }
  catch (const error &ex)
  {
    std::cerr << ex.what() << '\n';
  }
}
