# C++ Examples
---
C++ examples I use(d) for teaching students.

![Tamdandua Student](pijama_wuerfel.jpg)

## Examples
---
1. [Makefiles (generic Makefile and master Makefile).](makefiles/)
2. [I18N - translation with gnu gettext.](i18n/i18n.cpp)
3. [Radioactive Vampire Bunny Exercise from cplusplus.com.](radioactiveBunnyPopulation/)
4. [5 simple examples that show the usage of inheritance, (pure) virtual/abstract methods/classes and dynamic cast.](animalsInheritanceVirtualAbstractClassDynamicCastExample/src)
5. [Doxygen documentation example.](doxygenExample)
6. [C++ Error Handling (abort(); assert(), exceptions, return values etc.).](C++ErrorHandling/)
7. [XML files are often used for configuration: xml, dtd, and schema handling.](xml/)
8. [Debugging: #define _DEBUG_, gdb, strace etc.](debugging/)
9. [Testing: Unit testing with boost:test.](testing/)
10. [Libraries: static and shared libraries and dynamic loading etc.](libraries/)
11. [Parsing the commandline with boost::ProgramOptions.](commandlineParser/)
12. [Design patterns.](designPatterns/)
13. [Threads.](threads/)
14. [Templates.](templates/)
15. [Server and client sockets.](sockets/)
16. [Serial communication with an arduino nano.](serialPort/)
17. [Example uses std::unordered_map for mapping string to int which can be used in converting string to enums.](string2Enum/)
18. [Example shows how to implement a more modern C++ version of the singleton pattern.](singleton)
19. [Simple example for regular expressions in std C++.](regularExpressions/)


## References
---
* https://www.linuxhowtos.org/C_C++/socket.htm
* https://www.gnu.org/software/gettext/manual/html_node/gettext-grok.html
* http://www.cplusplus.com/forum/articles/12974/
* http://www.learncpp.com/cpp-tutorial/126-pure-virtual-functions-abstract-base-classes-and-interface-classes/

## Author:
---
* kaulquappe (https://bitbucket.org/kaulquappe/)
* Cubemast3r (https://bitbucket.org/Cubemast3r/)
