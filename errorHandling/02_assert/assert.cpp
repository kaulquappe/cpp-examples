/**
* Programm lets you type in your name and gives it out then.  
* When a non alphabetic character is entered the program calls assert(). 
* @see https://www.learncpp.com/cpp-tutorial/7-12a-assert-and-static_assert/
* @see https://stackoverflow.com/questions/1571340/what-is-the-assert-function
*/
#include <iostream>
#include <cassert>   // assert()

using namespace std;


bool isValidCharacter( char character )
{
    //cerr << "isValidCharacter(): character=\'" << static_cast<short>(character) << "\'\n";
    if ( character == 10 || character == 32 ) return true; // newline ('\n') and space
    else if ( 64 < character && character < 91 )   return true; // upper case letters 
    else if ( 96 < character && character < 123 )  return true; // lower case letters
    else return false;
}

int main(void)
{
    string name;
    char c;
    
    cout << "Enter your name: ";
    
    do
    {
    cin.get(c);
    // cerr << "Entered: " << c << endl;
    // assert(isValidCharacter(c));
    assert( isValidCharacter(c) && "Error: Invalid character! Please enter only alphabetic characters or space or newline!" );
    name += c;
    }
    while( c != '\n' );

    cout << "\nYour name is: " << name << '\n' << endl;

    return 0;
}
