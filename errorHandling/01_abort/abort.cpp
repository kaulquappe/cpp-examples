/**
* Programm lets you type in your name and gives it out then.  
* When a non alphabetic character is entered the program calls abort(). 
*/
#include <iostream>
#include <cstdlib>    // abort()

using namespace std;


bool isValidCharacter( char character )
{
    //cerr << "isValidCharacter(): character=\'" << static_cast<short>(character) << "\'\n";
    if ( character == 10 || character == 32 ) return true; // newline ('\n') and space
    else if ( 64 < character && character < 91 )   return true; // upper case letters 
    else if ( 96 < character && character < 123 )  return true; // lower case letters
    else return false;
}

int main(void)
{
    string name;
    char c;
    
    cout << "Enter your name: ";
    
    do
    {
    //cin >> std::noskipws >> c;
    cin.get(c);
    //cerr << "Entered: " << c << endl;
    if ( !isValidCharacter(c) ) abort();
    name += c;
    }
    while( c != '\n' );

    cout << "\nYour name is: " << name << '\n' << endl;

    return 0;
}
