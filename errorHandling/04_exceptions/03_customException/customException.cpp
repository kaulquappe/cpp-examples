/**
* Short program that shows how a custom exception can be made, that implements a kind of "call stack undwinding". 
* TODO: 
*       getNumberOfMessages(): returns how many messages/exceptions are on vecCustomExceptions_
*       getMessageNr( const unsigned int messageNr ): returns the message of the exception with the number messageNr ( counts from zero )
*       insert git Version in msg output
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct errorStruct 
{
    errorStruct( const char* msg, const char* file, const char* method, int line ) 
        : msg_(msg), file_(file), method_(method), line_(line)
    {
    }
    string msg_;
    string file_;
    string method_;
    int line_;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class customException 
{
public:
    customException( const char* msg, const char* file, const char* method, int line )
    {
        vecCustomExceptions_.push_back( errorStruct(msg, file, method, line) );
    }

    virtual ~customException() noexcept {};
    
    void add( const char* msg, const char* file, const char* method, int line )
    {
        vecCustomExceptions_.push_back( errorStruct(msg, file, method, line) );
    }
    
    const string getFirstMessage()
    {
        const errorStruct& es = vecCustomExceptions_.front();
        return es.file_+":"+to_string(es.line_)+":"+es.method_+"(): "+es.msg_+'\n';
    }

    const string getLastMessage()
    {
        const errorStruct& es = vecCustomExceptions_.back();
        return es.file_+":"+to_string(es.line_)+":"+es.method_+"(): "+es.msg_+'\n';
    }

    const string getCallStack()
    {
        string callStack;
        for (std::vector<errorStruct>::iterator it = vecCustomExceptions_.begin() ; it != vecCustomExceptions_.end(); ++it)
            callStack += '\t'+(*it).file_+":"+to_string((*it).line_)+":"+(*it).method_+"(): "+(*it).msg_+'\n';
        return callStack;
    }

protected:
        vector<errorStruct> vecCustomExceptions_; 
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void thirdMethod()
{
    throw( customException("Some ugly shit happened!!", __FILE__, __func__, __LINE__) );
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void secondMethod()
{
    try
    {
        thirdMethod();
    }
    catch( customException& cex)
    {
        cex.add( "Problem calling the third method ...", __FILE__, __func__, __LINE__ );
        throw( cex );
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void firstMethod()
{
    try
    {
        secondMethod();
    }
    catch( customException& cex )
    {
        cex.add( "Caught and exception!", __FILE__, __func__, __LINE__ );
        throw( cex );
    }    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(void)
{
    try
    {
        firstMethod();
    } 
    catch( customException& cex )
    {
        cerr << endl;
        cerr << "Main(): caught customException(): getFirstMessage():\n\t" << cex.getFirstMessage() << endl;
        cerr << "Main(): caught customException(): getLastMessage():\n\t"  << cex.getLastMessage()  << endl;
        cerr << "Main(): caught customException(): getCallStack():\n"      << cex.getCallStack()    << endl;
    }

    return 0;
}
