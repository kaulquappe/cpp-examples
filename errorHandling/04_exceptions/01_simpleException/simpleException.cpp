/**
* Short program to show how exceptions work.  
*/
#include <iostream>

using namespace std;


void getException( const char c )
{
    int intZwei = 2;
    unsigned short usDrei = 3;
    
    //cerr << "c: " << static_cast<unsigned short>(c) << '\n';

    // here different data types are thrown
    switch( c )
    {
        case(49): throw("1. char * exception."); break;
        case(50): throw(intZwei); break;
        case(51): throw(usDrei); break;
        case(52): throw(false); break;                  //throw unknown exception caught in catch(...)
        default: throw("Invalid number entered !!!");   //exception caught in const char * block
    }
}

int main(void)
{
    char c;
    try
    {
        cout << "Enter a number between 1 and 3: ";
        cin.get(c);
        getException( c );
     } // here different data types are caught
     catch( const char * msg)
     {
         cerr << '\n' << msg << '\n';
     }   
     catch( const int i)
     {
         cerr << i << ". int exception." << '\n';
     }   
     catch( const unsigned short us)
     {
         cerr << us << ". unsigned short exception." << '\n';
     }
     catch(...)  // catch all the rest
     {
         cerr << "Unknown exception caught!!\n";         
     }
    return 0;
}
