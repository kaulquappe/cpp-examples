/**
 * Contains an exception class that automatically includes the 
 * filename and the line number where the exception was thrown. 
 * 
 * @see File originally from https://github.com/mbedded-ninja/CppUtils.
 */
#ifndef _MACRO_EXCEPTION_H_
#define _MACRO_EXCEPTION_H_

#include <iostream>
#include <string>
#include <string.h>
#include <errno.h>

class MacroException : public std::runtime_error
{
public:
    MacroException(const char *file, int line, const std::string &arg) :
        std::runtime_error(arg)
    {
        msg_ = std::string(file) + ":" + std::to_string(line) + ": " + arg;
    }

    MacroException(const char *file, int line, int errnum, const std::string &arg) :
        std::runtime_error(arg)
    {
        msg_ = std::string(file) + ":" + std::to_string(line) + ": " + arg + " (" + strerror(errnum) + ").";
    }

    ~MacroException() throw() {}

    const char *what() const throw() override
    {
        return msg_.c_str();
    }

private:
    std::string msg_;
};

#define THROW_EXCEPT(arg)                throw MacroException(__FILE__, __LINE__, arg);
#define THROW_EXCEPT_ERRNO(arg, errnum ) throw MacroException(__FILE__, __LINE__, errnum, arg);

#endif
