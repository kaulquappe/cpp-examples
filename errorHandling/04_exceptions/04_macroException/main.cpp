#include <iostream>
#include "MacroException.h"
#include <fcntl.h> 


using namespace std;


void openWrongFile(const char* filename)
{
	int fd_ = ::open(filename, O_RDWR | O_NOCTTY | O_SYNC);
	//if (fd_ < 0) THROW_EXCEPT( std::string("Could not open \"") + filename + "\"" )
	// 0R:   
	if (fd_ < 0) THROW_EXCEPT_ERRNO( std::string("Could not open \"") + filename + "\"" , errno )
}

int main()
{
    try
    {
		openWrongFile("IdoNotExist.txt");
    }
    catch (MacroException& ex)
    {
        cerr << "ERROR: " << ex.what() << endl;
        return 1;
    }
    catch (std::exception& ex)
    {
        cerr << "ERROR: " << ex.what() << endl;
        return 1;
    }

    return 0;
}



