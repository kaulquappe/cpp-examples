/**
* Short program to show how std exceptions work.  
*/
#include <iostream>
#include <exception>
#include <string>

using namespace std;


struct firstException : public exception 
{
    firstException() : ::exception(), msg_("firstException() thrown!") {};
    firstException(const char * msg) : ::exception(), msg_(msg) {};
    const char * what() const throw () 
    {
      return msg_.c_str();
    }
      
    const string msg_;   
};

/*
struct secondException : public exception 
{
   const char * what () const throw () 
   {
      return msg;
   }
   ::msg = "secondException!"
};
*/

struct secondException : public exception 
{
   const char * what () const throw () 
   {
      return "secondException(): some shit happened here!";
   }
};

void secondMethod()
{
    throw( secondException() );
}

void firstMethod()
{
    try
    {
        secondMethod();
    }
    catch( const secondException& sex )
    {
        // cerr << "firstMethod() caught an secondException: " << sex.what() << endl;
        cout << "\nType in your choice: \n\t"
                "1: throw firstException() \n\t"
                "2: throw secondException() \n\t"
                "3: throw exception( firstException() ) \n\t"
                "4: throw exception( secondException() ) \n\t"
                "5: throw exception() \n\t"
                "6: throw \"firstMethod() caught and secondException: \" \n\t"
                "7: throw 4 \n\t" 
                "8: exit program" << endl; 
        int choice;
        cin >> choice;
        
        switch(choice) {
                case 1: throw firstException();
                case 2: throw secondException();
                case 3: throw exception( firstException() );
                case 4: throw exception( secondException() );
                case 5: throw exception();
                case 6: throw "firstMethod() caught and secondException: ";
                case 7: throw 4;
                case 8: throw 8;
                default: throw firstException("unknown choice: allowed values are integer 1 to 8");
        }
    }    
}

int main(void)
{
    do 
    {
        try
        {
            firstMethod();
        } 
        catch( const firstException& fex )
        {
            cerr << "Main(): caught firstException(): " << fex.what() << endl;
        }   
        catch( const secondException& sex )
        {
            cerr << "Main(): caught secondException(): " << sex.what() << endl;
        }   
        catch( const exception& ex )
        {
            cerr << "Main(): caught exception(): " << ex.what() << endl;
        }
        catch( const int i ) 
        {
            if (i==8) break;
            cerr << "Caught integer exception: " << i << endl;
        } 
        catch(...)  // catch all the rest
        {
            cerr << "Unknown exception caught!!\n";         
        }
    } while(true);
    
    return 0;
}
