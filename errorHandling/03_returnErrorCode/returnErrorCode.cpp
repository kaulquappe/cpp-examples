/**
* Programm lets you type in your name and gives it out then.  
* When a non alphabetic character is entered the program calls gives back an error code that is associated with the character entered. 
* @Attention note the use of 'swicht' and 'if else'
* @Attention note the use enum returnCode as return value from main().
* @Attention use "./returnErrorCode.exe; echo $?" in order to see return code in shell
*/
#include <iostream>
#include <ctype.h>   // isdigit(), isalpha(), isspace(), iscntrl(), 

using namespace std;
enum returnCode 
{
    NEWLINE=0,     // newline ('\n') 
    ALPHA=1,       // alphabetic letters
    DIGIT=2,       // digits of a number 
    SPACE=3,       // space
    SPACE_NOK=4,   // all space except newline ('\n') and space
    PUNCT=5,       // punctuation character
    CTRL=6,        // control character
    UNSPECIFIED=7  // the rest: special characters etc.
};

returnCode isValidCharacter( char character )
{
    if ( isalpha(character) ) return returnCode::ALPHA;      // alphabetic letters     
    else if ( character == 10 ) return returnCode::NEWLINE;  // newline ('\n')
    else if ( character == 32 ) return returnCode::SPACE;    // newline ('\n') and space
    else if ( isspace(character) ) return returnCode::SPACE_NOK; // space that is not allowed (all except newline ('\n') and space)
    else if ( isdigit(character) ) return returnCode::DIGIT; // digits
    else if ( ispunct(character) ) return returnCode::PUNCT; // punctuation character
    else if ( iscntrl(character) ) return returnCode::CTRL;  // control character
    else return returnCode::UNSPECIFIED;
}

int main(void)
{
    string name;
    char c;
    returnCode code;
    
    cout << "Enter your name: ";
    
    do
    {
        cin.get(c);        
        code = isValidCharacter(c);
        switch( code )
        {
            case( returnCode::ALPHA ):       name += c; break;
            case( returnCode::SPACE ):       name += c; ; break;
            case( returnCode::NEWLINE ):     break;
            case( returnCode::DIGIT ):       cerr << "Error: no digits are allowed in names!\n"; return returnCode::DIGIT;
            case( returnCode::PUNCT ):       cerr << "Error: no punctuation characters are allowed in names!\n"; return returnCode::PUNCT;
            case( returnCode::SPACE_NOK ):   cerr << "Error: no space characters except [SPACE] or /n are allowed in names!\n"; return returnCode::SPACE_NOK;
            case( returnCode::CTRL ):        cerr << "Error: no control characters are allowed in names!\n"; return returnCode::CTRL;
            case( returnCode::UNSPECIFIED ): cerr << "Error: only alphabetic characters and space is allowed in names!\n"; return returnCode::UNSPECIFIED;
            default: cerr << "Error: unknown character entered: \'" << c << "\' !!\n"; break; 
        }
    }
    while( code == returnCode::ALPHA || code == returnCode::SPACE );
    
    cout << "\nYour name is: " << name << '\n' << endl;
    
    return code;
}

/**
    do
    {
        cin.get(c);
        // cerr << "Entered: " << c << endl;
        
        code = isValidCharacter(c);
        if ( code == returnCode::ALPHA || code == returnCode::SPACE_OK ) name += c;
        else if ( code == returnCode::DIGIT )
        {
            cerr << "Error: no digits are allowed in names!\n";
            return returnCode::DIGIT;
        }
        else if ( code == returnCode::PUNCT )
        {
            cerr << "Error: no punctuation characters are allowed in names!\n";
            return returnCode::PUNCT;
        }
        else if ( code == returnCode::SPACE_NOK )
        {
            cerr << "Error: no space characters except [SPACE] or /n are allowed in names!\n";
            return returnCode::SPACE_NOK;
        }
        else if ( code == returnCode::CTRL )
        {
            cerr << "Error: no control characters are allowed in names!\n";
            return returnCode::CTRL;
        }
        else if ( code == returnCode::UNSPECIFIED )
        {
            cerr << "Error: only alphabetic characters and space is allowed in names!\n";
            return returnCode::UNSPECIFIED;
        }
    }
    while( c != '\n' );
**/
