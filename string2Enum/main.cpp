//  g++ -g -Wall -o enumTest.exe main.cpp 
#include <iostream>
#include <unordered_map>

using namespace std;

enum color
{
    blue = 1,
    red=2,
    green=3,
    yellow=4,
};

int main()
{
    color myColor = color::red;

    switch (myColor)
    {
        case red:
            cout << "\n-> red" << endl;
            break;
         case blue:
            cout << "\n-> blue:" << endl;
            break;
         case green:
            cout << "\n-> green:" << endl;
            break;
         case yellow:
            cout << "\n-> yellow:" << endl;
            break;
    }
    
    std::unordered_map<std::string,int> mColors { {"blue",1},{"red",2},{"green",3},{"yellow",4} };
    cout << endl;
    cout << "yellow:\t" << mColors["yellow"] << endl;
    cout << "green:\t" << mColors["green"] << endl;
    cout << "red:\t" << mColors["red"] << endl;
    cout << "blue:\t" << mColors["blue"] << endl;
    cout << endl;

    for(auto p : mColors) cout << '{' << p.first << ',' << p.second << "}\n";
    cout << endl;
    mColors.clear();

    return 0;
}
