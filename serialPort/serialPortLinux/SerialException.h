/**
 * Contains the Exception class. File originally from https://github.com/mbedded-ninja/CppUtils.
 */
#ifndef _SERIAL_EXCEPTION_H_
#define _SERIAL_EXCEPTION_H_

#include <iostream>
#include <string>
#include <string.h>
#include <errno.h>

class SerialException : public std::runtime_error
{

public:
    SerialException(const char *file, int line, const std::string &arg) :
        std::runtime_error(arg)
    {
        msg_ = std::string(file) + ":" + std::to_string(line) + ": " + arg;
    }

    SerialException(const char *file, int line, int errnum, const std::string &arg) :
        std::runtime_error(arg)
    {
        msg_ = std::string(file) + ":" + std::to_string(line) + ": " + arg + " (" + strerror(errnum) + ").";
    }

    ~SerialException() throw() {}

    const char *what() const throw() override
    {
        return msg_.c_str();
    }

private:
    std::string msg_;
};
#define THROW_EXCEPT(arg)               throw SerialException(__FILE__, __LINE__, arg);
#define THROW_EXCEPT_ERRNO(arg, errnum ) throw SerialException(__FILE__, __LINE__, errnum, arg);

#endif // _LINUX_SERIAL_EXCEPTION_H_
