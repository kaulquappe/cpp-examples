/****************************************************************************
**
** Copyright (C) 2020 Philipp Zwetkoff and Arno Wilhelm
** Contact: https://bitbucket.org/pidWorkbenchTeam/pid-workbench_repository/issues
**
** This file is part of PidWorkbench.
**
** This file may be used under the terms of the GNU General Public License 
** version 3 as published by the Free Software Foundation. 
** Please review the following information to ensure the 
** GNU General Public License requirements will be met: 
** https://www.gnu.org/licenses/gpl-3.0.html.
**
****************************************************************************/

#include "Serial.h"
#include "SerialException.h"
#include <unistd.h>  // sleep
#include <fcntl.h> 

#define  NONE	0000000  // value for empty flags 

std::unordered_map<std::string, speed_t>
    Serial::Baudrate 
    { 
        { "B50", B50 },
        { "B75", B75 },
        { "B110", B110 },
        { "B134", B134 },
        { "B150", B150 },
        { "B200", B200 },
        { "B300", B300 },
        { "B600", B600 },
        { "B1200", B1200 },
        { "B1800", B1800 },
        { "B2400", B2400 },
        { "B4800", B4800 },
        { "B9600", B9600 },
        { "B19200", B19200 },
        { "B38400", B38400 },  
        { "B57600", B57600 },
        { "B115200", B115200 },
        { "B230400", B230400 },
        { "B460800", B460800 },
        { "B500000", B500000 },
        { "B576000", B576000 },
        { "B921600", B921600 },
        { "B1000000", B1000000 },
        { "B1152000", B1152000 },
        { "B1500000", B1500000 },
        { "B1500000", B1500000 },
        { "B2000000", B2000000 },
        { "B2500000", B2500000 },
        { "B3000000", B3000000 },
        { "B3500000", B3500000 },
        { "B4000000", B4000000 }
    };
    
    std::unordered_map<std::string, tcflag_t>
    Serial::Csize 
    { 
        { "CS5", CS5 },
        { "CS6", CS6 },
        { "CS7", CS7 },
        { "CS8", CS8 }
    };

std::unordered_map<std::string, tcflag_t> 
    Serial::Parity 
    { 
        { "none", NONE},
        { "odd", (PARENB | PARODD) },
        { "even", PARENB }
    };
               
std::unordered_map<std::string, tcflag_t>
    Serial::StopBits 
    { 
        { "one", NONE },
        { "two", CSTOPB }
    };
                    
Serial::Serial() : fd_(-1), seconds_(5.0)
{
    ;
}

Serial::~Serial()
{
    if (fd_ > 0) ::close(fd_);
}

void Serial::open(const char* const device, 
         const char* const baudrate,
         const char* const csize,
         const char* const parity,
         const char* const stopBit)
{
    struct termios tty;

    if (fd_ > 0) ::close(fd_);
  
    fd_ = ::open(device, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd_ < 0) THROW_EXCEPT_ERRNO( std::string("Could not open ") + device , errno ) 
    if (tcgetattr(fd_, &tty) < 0) THROW_EXCEPT_ERRNO( std::string("tcgetattr() failed! "), errno )
  
    cfsetospeed(&tty, Serial::Baudrate[baudrate]);
    cfsetispeed(&tty, Serial::Baudrate[baudrate]);
 
    tty.c_cflag |= (CLOCAL | CREAD);          // ignore modem controls
    tty.c_cflag &= ~CSIZE;                    // clear character size flags
    tty.c_cflag |= Serial::Csize[csize] ;     // set new character size
    tty.c_cflag &= ~(PARENB | PARODD);        // clear parity flags
    tty.c_cflag |= Serial::Parity[parity];    // set new parity flag 
    tty.c_cflag &= ~CSTOPB;                   // clear stop bit
    tty.c_cflag |= Serial::StopBits[stopBit]; // set new stop bit
    tty.c_cflag &= ~CRTSCTS;                  // no hardware flowcontrol
    tty.c_cflag &= ~HUPCL;                    // Lower modem control lines after last process closes the device (hang up). 
    
    // setup for non-canonical mode
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    // wait VTIME for incoming data -> else time out
    tty.c_cc[VMIN]  = 0;     // Minimum number of characters to read
    tty.c_cc[VTIME] = seconds_*10;   // Time to wait for data (tenths of seconds)

    if (tcsetattr(fd_, TCIOFLUSH, &tty) != 0) THROW_EXCEPT_ERRNO( std::string("tcsetattr() failed! "), errno );
    // delay here is necessary in order to take the call to tcsetattr()
    // above effect when reading from the serial the first time !!
    sleep(2);
}
   
void Serial::write(const char* const msg)
{
    if ( ::write(fd_, msg, strlen(msg)) != strlen(msg) )
    {
        std::string errorStr("Writing \"");
        errorStr += msg; 
        errorStr += "\" so serial device failed! ";
        THROW_EXCEPT_ERRNO( errorStr, errno );
    }
    tcdrain(fd_);    // delay for output 
}

std::string Serial::read()
{
    dataRead_ = "";
    // read as long as there is nothing to read anymore
    do
    {
        int readLen_ = ::read(fd_, readBuf_, BUFSIZE - 1);
        if (readLen_ > 0) 
        {
            readBuf_[readLen_] = 0;
            dataRead_ += readBuf_;
        } 
        else if (readLen_ < 0) 
        { 
            THROW_EXCEPT_ERRNO( std::string("Reading from serial port failed! "), errno );
        }
        else // readLen_ == 0
        {
            THROW_EXCEPT_ERRNO( std::string("Timout from serial read! "), errno );  
        }
    } while (readLen_ == (BUFSIZE - 1) );
    
    return dataRead_;
}

void Serial::close()
{
    if (fd_ < 0)
    {
        ::close(fd_);
        fd_=-1;
    }
}

void Serial::setTimeout(const float seconds)
{
    seconds_ = seconds;
}
