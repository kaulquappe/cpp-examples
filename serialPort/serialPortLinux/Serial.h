/****************************************************************************
**
** Copyright (C) 2020 Philipp Zwetkoff and Arno Wilhelm
** Contact: https://bitbucket.org/pidWorkbenchTeam/pid-workbench_repository/issues
**
** This file is part of PidWorkbench.
**
** This file may be used under the terms of the GNU General Public License 
** version 3 as published by the Free Software Foundation. 
** Please review the following information to ensure the 
** GNU General Public License requirements will be met: 
** https://www.gnu.org/licenses/gpl-3.0.html.
**
****************************************************************************/

#ifndef __SERIAL_H__
#define __SERIAL_H__

#include <string>
#include <unordered_map>
#include "termios.h"

#define BUFSIZE 256

/**
  * @class Serial
  * Class handles opening, closing, reading and writing to a serial device.
  * @see https://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c
  * @see PidWorkbenchSerialConnection
  * 
  * @author  Arno Wilhelm & Philipp Zwetkoff
  * @version 1.0
  * @since   2020-08-13
  */
class Serial
{
public:
    Serial();
    
    ~Serial();
    
    void open(const char* const device, 
              const char* const baudrate,
              const char* const csize="CS8",
              const char* const parity="none",
              const char* const stopBit="one");
    
    void write(const char* const msg);
    
    /**
     * Reads data from the serial device.
     * @return string with data read from serial device.
     * @throws SerialException when an error occured or read timed out.
     * @todo Should we make a special timeout exception here ? 
     */
    std::string read();
   
    void close();
    /**
     * Sets the timeout for reading from the serial device.
     * 
     * @param seconds to wait when reading data before the read call returns. 
     * @notice up to 1/10 of seconds are allowed: eg.: 2.5 is a valid value.
     * @attention use setTimeout() *before* you open the device!
     * @see Serial::open
     */
    void setTimeout(const float seconds);
    
    // mapping strings termios.h datatypes/constants for serial communication
    static std::unordered_map<std::string, speed_t> Baudrate;
    static std::unordered_map<std::string, tcflag_t> Csize;
    static std::unordered_map<std::string, tcflag_t> Parity;
    static std::unordered_map<std::string, tcflag_t> StopBits;
    //static std::unordered_map<std::string, tcflag_t> flow_control;   // always none for now
    
private:
    /**
     * File handle for serial port.
     */
    int fd_;
    /**
     * Serial read timeout in seconds.
     * Values up to 1/10 of a second are valid (eg.: 2.5).
     */
    float seconds_;
    /**
     * Serial read buffer.
     */
    char readBuf_[BUFSIZE];
     /**
     * number of bytes read from serial
     */
    int readLen_;
    /**
     * Data that was read from the serial device is returned in this string.
     */
    std::string dataRead_;
};

#endif //__SERIAL__
