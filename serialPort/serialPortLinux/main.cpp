#include <iostream>
#include "Serial.h"
#include "SerialException.h"

using namespace std;

int main()
{
    try
    {
        Serial mySerial;
        mySerial.setTimeout(5000);

        mySerial.open("/dev/ttyUSB0", "B9600");

        mySerial.write("Hi du Ei!");
        cerr << mySerial.read();

        mySerial.write("Wie gehts dir?");
        cerr << mySerial.read();

        mySerial.write("Gut! Und dir?");
        cerr << mySerial.read();

        mySerial.close();  
    }
    catch (SerialException& ex)
    {
        cerr << "ERROR: " << ex.what() << endl;
        return 1;
    }
    catch (std::exception& ex)
    {
        cerr << "ERROR: " << ex.what() << endl;
        return 1;
    }

    return 0;
}
