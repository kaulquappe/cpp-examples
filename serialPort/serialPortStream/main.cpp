/* This example was used as a test to communicate with an arduino nano that was
 * loaded with the following serialEcho program:

    String strRead;

    void setup() 
    {
      // Turn the Serial Protocol ON
      Serial.begin(9600);
    }

    void loop() 
    {
       //  check if data has been sent from the computer:
      if (Serial.available()) 
      {
        strRead = Serial.readString();
        delay(500);
        Serial.println(strRead);
      }
    }
 *
 * @see https://gist.github.com/kaliatech/427d57cb1a8e9a8815894413be337cf9
 * @see https://github.com/fedetft/serial-port
 * @see https://www.arduino.cc/reference/de/language/functions/communication/serial/begin/
 * @attention: in linux we needed to issue following command in order not to get a timeout:  > stty -F /dev/ttyUSB0 -hupcl
 **/
 

#include <iostream>
#include "serialstream.h"

using namespace std;
using namespace boost::posix_time;

int main()
{    
    // Setting up the serial communication with arduino
    SerialOptions options;
    options.setDevice("/dev/ttyUSB0");
    options.setTimeout(seconds(8));
    options.setFlowControl(SerialOptions::noflow);

    // Arduino default: 9600 baud: 8N1 (=Daten,Parität,Stoppbits)
    options.setBaudrate(9600);
    options.setCsize(8);
    options.setParity(SerialOptions::noparity);
    options.setStopBits(SerialOptions::one);

    SerialStream serial(options);
    serial.exceptions(ios::badbit | ios::failbit); //Important!

    serial << "Hello World!" << endl;
    try
    {
        string s;
        //serial>>s;
        //sleep(2);
        getline(serial,s);
        cout << s << endl;
    }
    catch(TimeoutException&)
    {
        serial.clear(); //Don't forget to clear error flags after a timeout
        cerr << "Timeout occurred" << endl;
    }
    catch(std::exception& e)
    {
        cerr << "ERROR: " << e.what() << endl;
    }
    serial.close();
    return 0;
}
