/* This example was used as a test to communicate with an arduino nano that was
 * loaded with the following serialEcho program:

    String strRead;

    void setup() 
    {
      // Turn the Serial Protocol ON
      Serial.begin(9600);
    }

    void loop() 
    {
       //  check if data has been sent from the computer:
      if (Serial.available()) 
      {
        strRead = Serial.readString();
        delay(500);
        Serial.println(strRead);
      }
    }
 *
 * @see https://gist.github.com/kaliatech/427d57cb1a8e9a8815894413be337cf9
 * @see https://github.com/fedetft/serial-port
 * @see https://www.arduino.cc/reference/de/language/functions/communication/serial/begin/
 * @attention: in linux we needed to issue following command in order not to get a timeout:  > stty -F /dev/ttyUSB0 -hupcl
 **/
 
#include <iostream>
#include "Serial.h"

using namespace std;
using namespace boost;

int main()
{   
    try 
    {
        Serial serial;
        serial.open("/dev/ttyUSB0", 9600, 
                asio::serial_port_base::character_size(8),
                Serial::parity["none"],
                Serial::stop_bits["one"],
                Serial::flow_control["none"]
        );
        
        cerr << "boost::asio::serial_port_base::parity::none: " << static_cast<int>(boost::asio::serial_port_base::parity::none) << endl;
        cerr << "boost::asio::serial_port_base::parity::odd: " << static_cast<int>(boost::asio::serial_port_base::parity::odd) << endl;
        cerr << "boost::asio::serial_port_base::parity::even: " << static_cast<int>(boost::asio::serial_port_base::parity::even) << endl;
        cerr << "boost::asio::serial_port_base::stop_bits::one         : " << static_cast<int>(boost::asio::serial_port_base::stop_bits::one) << endl;
        cerr << "boost::asio::serial_port_base::stop_bits::onepointfive: " << static_cast<int>(boost::asio::serial_port_base::stop_bits::onepointfive) << endl;
        cerr << "boost::asio::serial_port_base::stop_bits::two         : " << static_cast<int>(boost::asio::serial_port_base::stop_bits::two) << endl;
        cerr << "boost::asio::serial_port_base::flow_control::none    : " << static_cast<int>(boost::asio::serial_port_base::flow_control::none) << endl;
        cerr << "boost::asio::serial_port_base::flow_control::software: " << static_cast<int>(boost::asio::serial_port_base::flow_control::software) << endl;
        cerr << "boost::asio::serial_port_base::flow_control::hardware: " << static_cast<int>(boost::asio::serial_port_base::flow_control::hardware) << endl;
        
        serial.setTimeout(posix_time::seconds(5));

        //Text test
        serial.writeString("Hello world\n");
        cout << serial.readStringUntil("\n") << endl;
    
        //Binary test
        unsigned char values[]={0xde,0xad,0xbe,0xef};
        serial.write(reinterpret_cast<char*>(values),sizeof(values));
        serial.read(reinterpret_cast<char*>(values),sizeof(values));
        for(unsigned int i=0;i<sizeof(values);i++)
        {
            cout<< static_cast<int>(values[i]) << endl;
        }

        serial.close();  
    } 
    catch(boost::system::system_error& e)
    {
        cout<< "Error: " << e.what() << endl;
        return 1;
    }
    return 0;
}
