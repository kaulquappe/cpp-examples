/*
 * File:   TimeoutSerial.cpp
 * Author: Terraneo Federico
 * Distributed under the Boost Software License, Version 1.0.
 * Created on September 12, 2009, 3:47 PM
 *
 * v1.06: C++11 support
 *
 * v1.05: Fixed a bug regarding reading after a timeout (again).
 *
 * v1.04: Fixed bug with timeout set to zero
 *
 * v1.03: Fix for Mac OS X, now fully working on Mac.
 *
 * v1.02: Code cleanup, speed improvements, bug fixes.
 *
 * v1.01: Fixed a bug that caused errors while reading after a timeout.
 *
 * v1.00: First release.
 */

#include "Serial.h"
#include <string>
#include <algorithm>
#include <iostream>
#include <boost/bind.hpp>

using namespace std;
using namespace boost;
using namespace boost::asio;

std::unordered_map<std::string,boost::asio::serial_port_base::parity> 
    Serial::parity 
    { 
        { "none", boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none)},
        { "odd", boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::odd)},
        { "even", boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::even)} 
    };

std::unordered_map<std::string,boost::asio::serial_port_base::stop_bits> 
    Serial::stop_bits 
    { 
        { "one", boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one)},
        { "onepointfive", boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::onepointfive)},
        { "two", boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::two)}
    };
                
std::unordered_map<std::string, boost::asio::serial_port_base::flow_control> 
    Serial::flow_control
    {
        { "none", boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none)},
        { "software", boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::software)},
        { "hardware", boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::hardware)},
    };
    
Serial::Serial(): io_(), port_(io_), timer_(io_),
        timeout_(boost::posix_time::seconds(0)) {}

Serial::Serial(const std::string& devname, unsigned int baud_rate,
        asio::serial_port_base::character_size opt_csize,
        asio::serial_port_base::parity opt_parity,
        asio::serial_port_base::stop_bits opt_stop,
        asio::serial_port_base::flow_control opt_flow)
        : io_(), port_(io_), timer_(io_), timeout_(boost::posix_time::seconds(0))
{
    open(devname,baud_rate,opt_csize,opt_parity,opt_stop,opt_flow);
}

void Serial::open(const std::string& devname, unsigned int baud_rate,
        asio::serial_port_base::character_size opt_csize,
        asio::serial_port_base::parity opt_parity,
        asio::serial_port_base::stop_bits opt_stop,
        asio::serial_port_base::flow_control opt_flow )
{
    if(isOpen()) close();
    port_.open(devname);
    
#ifdef __unix__
        /**
         * HUPCL: Lower modem control lines after last process closes the device (hang up).
         * @attention in linux we needed to issue following command in order not to get a timeout:  > stty -F /dev/ttyUSB0 -hupcl
         * @see man tcsetattr
         * @see https://stackoverflow.com/questions/26793878/boost-asio-for-high-baud-rate-serial-on-os-x
         * @see http://boost.2283326.n4.nabble.com/Simple-serial-port-demonstration-with-boost-asio-asynchronous-I-O-td2582657.html
         */
        serial_port::native_handle_type fd = port_.native_handle();
        termios settings;
        if (tcgetattr(fd, &settings) < 0) perror("tcgetattr()");
        // settings.c_lflag &= (~ICANON);
        // settings.c_lflag &= (~ISIG);
        // if (cfsetspeed(&settings, 9600) < 0) { /* handle error */ }
        settings.c_cflag &= (~HUPCL);
        if (tcsetattr(fd, TCSANOW, &settings) < 0) perror("tcsetattr()");
        sleep(2);   // we need to sleep here because otherwise the HUBCL does not take effect at time.
#endif 
    
    port_.set_option(asio::serial_port_base::baud_rate(baud_rate));
    port_.set_option(opt_parity);
    port_.set_option(opt_csize);
    port_.set_option(opt_flow);
    port_.set_option(opt_stop);
}

bool Serial::isOpen() const
{
    return port_.is_open();
}

void Serial::close()
{
    if(isOpen()==false) return;
    port_.close();
}

void Serial::setTimeout(const boost::posix_time::time_duration& t)
{
    timeout_=t;
}

void Serial::write(const char *data, size_t size)
{
    asio::write(port_,asio::buffer(data,size));
}

void Serial::write(const std::vector<char>& data)
{
    asio::write(port_,asio::buffer(&data[0],data.size()));
}

void Serial::writeString(const std::string& s)
{
    asio::write(port_,asio::buffer(s.c_str(),s.size()));
}

void Serial::read(char *data, size_t size)
{
    if(readData_.size()>0)//If there is some data from a previous read
    {
        istream is(&readData_);
        size_t toRead=min(readData_.size(),size);//How many bytes to read?
        is.read(data,toRead);
        data+=toRead;
        size-=toRead;
        if(size==0) return;//If read data was enough, just return
    }
    
    setupParameters_=ReadSetupParameters(data,size);
    performReadSetup(setupParameters_);

    //For this code to work, there should always be a timeout, so the
    //request for no timeout is translated into a very long timeout
    if(timeout_!=boost::posix_time::seconds(0)) timer_.expires_from_now(timeout_);
    else timer_.expires_from_now(boost::posix_time::hours(100000));
    
    timer_.async_wait(boost::bind(&Serial::timeoutExpired,this,
                asio::placeholders::error));
      
    result_=resultInProgress;
    bytesTransferred_=0;
    for(;;)
    {
        io_.run_one();
        switch(result_)
        {
            case resultSuccess:
                timer_.cancel();
                return;
            case resultTimeoutExpired:
                port_.cancel();
                throw(timeout_exception("Timeout expired"));
            case resultError:
                timer_.cancel();
                port_.cancel();
                throw(boost::system::system_error(boost::system::error_code(),
                        "Error while reading"));
            case resultInProgress:
                break;
            //if resultInProgress remain in the loop
        }
    }
}

std::vector<char> Serial::read(size_t size)
{
    vector<char> result(size,'\0');//Allocate a vector with the desired size
    read(&result[0],size);//Fill it with values
    return result;
}

std::string Serial::readString(size_t size)
{
    string result(size,'\0');//Allocate a string with the desired size
    read(&result[0],size);//Fill it with values
    return result;
}

std::string Serial::readStringUntil(const std::string& delim)
{
    // Note: if readData contains some previously read data, the call to
    // async_read_until (which is done in performReadSetup) correctly handles
    // it. If the data is enough it will also immediately call readCompleted()
    setupParameters_=ReadSetupParameters(delim);
    performReadSetup(setupParameters_);

    //For this code to work, there should always be a timeout, so the
    //request for no timeout is translated into a very long timeout
    if(timeout_!=boost::posix_time::seconds(0)) timer_.expires_from_now(timeout_);
    else timer_.expires_from_now(boost::posix_time::hours(100000));

    timer_.async_wait(boost::bind(&Serial::timeoutExpired,this,
                asio::placeholders::error));

    result_=resultInProgress;
    bytesTransferred_=0;
    for(;;)
    {
        io_.run_one();
        switch(result_)
        {
            case resultSuccess:
                {
                    timer_.cancel();
                    bytesTransferred_-=delim.size();//Don't count delim
                    istream is(&readData_);
                    string result(bytesTransferred_,'\0');//Alloc string
                    is.read(&result[0],bytesTransferred_);//Fill values
                    is.ignore(delim.size());//Remove delimiter from stream
                    return result;
                }
            case resultTimeoutExpired:
                port_.cancel();
                throw(timeout_exception("Timeout expired"));
            case resultError:
                timer_.cancel();
                port_.cancel();
                throw(boost::system::system_error(boost::system::error_code(),
                        "Error while reading"));
            case resultInProgress:
                break;
            //if resultInProgress remain in the loop
        }
    }
}

Serial::~Serial() {}

void Serial::performReadSetup(const ReadSetupParameters& param)
{
    if(param.fixedSize_)
    {
        asio::async_read(port_,asio::buffer(param.data_,param.size_),boost::bind(
                &Serial::readCompleted,this,asio::placeholders::error,
                asio::placeholders::bytes_transferred));
    } else {
        asio::async_read_until(port_,readData_,param.delim_,boost::bind(
                &Serial::readCompleted,this,asio::placeholders::error,
                asio::placeholders::bytes_transferred));
    }
}

void Serial::timeoutExpired(const boost::system::error_code& error)
{
     if(!error && result_==resultInProgress) result_=resultTimeoutExpired;
}

void Serial::readCompleted(const boost::system::error_code& error,
        const size_t bytesTransferred)
{
    if(!error)
    {
        result_=resultSuccess;
        this->bytesTransferred_=bytesTransferred;
        return;
    }

    //In case a asynchronous operation is cancelled due to a timeout,
    //each OS seems to have its way to react.
    #ifdef _WIN32
    if(error.value()==995) return; //Windows spits out error 995
    #elif defined(__APPLE__)
    if(error.value()==45)
    {
        //Bug on OS X, it might be necessary to repeat the setup
        //http://osdir.com/ml/lib.boost.asio.user/2008-08/msg00004.html
        performReadSetup(setupParameters_);
        return;
    }
    #else //Linux
    if(error.value()==125) return; //Linux outputs error 125
    #endif

    result_=resultError;
}
