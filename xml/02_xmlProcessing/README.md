# Transform a given xml file with a given .xslt file with the .xslt processor 'xstlproc':

```
>  xsltproc xsltTest.xsl xsltTest.xml > xsltTest.html
```

# References

* https://www.w3schools.com/xml/xml_xslt.asp
* https://www.w3schools.com/xml/xsl_intro.asp
