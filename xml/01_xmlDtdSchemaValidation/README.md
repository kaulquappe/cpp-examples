# Validate against a dtd:

```
> xmllint --noout --dtdvalid xmlTest.dtd xmlTest.xml

> xmllint --dtdvalid xmlTest.dtd xmlTest.xml
```
# Validate against a schema file:

```
> xmllint --schema xmlTest.xsd xmlTest.xml
```

# References

* https://stackoverflow.com/questions/29072962/xmllint-how-to-validate-an-xml-using-a-local-dtd-file
* https://www.w3schools.com/xml/xml_dtd.asp
* https://www.w3schools.com/xml/xml_schema.asp

