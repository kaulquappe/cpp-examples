/**
 * Example shows the usage of std::thread.
 * There is concurrency in the thread execution because the threads spend
 * their time in one single function. In order to synchronize the threads
 * they are locked with std::lock_guard.
 *
 * @note The output was generated using a unix terminal with black background!
 * @note Dont forget to put "-pthread" into your g++ linker options on linux!
 * @note compile with: g++ -g -Wall -pthread -o concurrentThread.exe concurrentThread.cpp
 * @see http://www.cplusplus.com/reference/thread/thread/
 * @see http://www.cplusplus.com/reference/thread/thread/get_id/
 * @see https://stackoverflow.com/questions/266168/simple-example-of-threading-in-c
 * @see http://www.cplusplus.com/reference/mutex/mutex/lock/
 * @see http://www.cplusplus.com/reference/mutex/lock_guard/
 * @see http://www.cplusplus.com/reference/mutex/mutex/try_lock/
 * @see http://www.cplusplus.com/reference/chrono/
 * @see http://www.cplusplus.com/reference/thread/this_thread/sleep_for/
 * @see https://en.wikipedia.org/wiki/ANSI_escape_code#graphics
 * */
#include <stdlib.h>       // srand, rand
#include <iostream>       // std::cout
#include <thread>         // std::thread
#include <mutex>          // std::mutex
#include <chrono>         // for sleeping

using namespace std;

std::mutex mtx;           // mutex for critical section
const int numThreads=7; // 7 console colors available when background is black !!

// simulates workload on single thread
void randomSleep()
{
    srand (time(NULL));
    // sleep between 1 to 100 miliseconds
    //for (int i=0; i< 100; ++i)
    std::this_thread::sleep_for(std::chrono::milliseconds(rand() % 200 + 1));
}
// unlocking version
void printThreadIdUnlocked(int threadNr)
{
    string myColorBegin = "\033[0;9" + to_string(threadNr) + ";40m";
    string myColorEnd   = "\033[0m";

    cerr << myColorBegin << "\nThread #" << threadNr << myColorEnd << ": ";
    for (int i=0; i<10; ++i)
    {
        randomSleep();
        cerr << myColorBegin << "*" << myColorEnd;
    }
}

// locking version
void printThreadIdLocked(int threadNr)
{
    // critical section (exclusive access to std::cout signaled by locking mtx):
    std::lock_guard<std::mutex> lock (mtx);
    string myColorBegin = "\033[0;9" + to_string(threadNr) + ";40m";
    string myColorEnd   = "\033[0m";

    cerr << myColorBegin << "\nThread #" << threadNr << myColorEnd << ": ";
    for (int i=0; i<10; ++i)
    {
        randomSleep();
        cerr << myColorBegin << "*" << myColorEnd;
    }
}

int main ()
{
    std::thread threads[numThreads];
    cout << "--> Lets spawn 7 threads and let them print out colorful stars in an *unlocked* operation:" << endl;
    for (int i=0; i<numThreads; ++i)
        threads[i] = std::thread(printThreadIdUnlocked,i+1);
    for (auto& th : threads) th.join(); // join threads again ...
    cout << "\n--> This generated probably a whole lot of mess ..." << endl;

    cout << "\n--> Now we spawn 7 threads again and let them print out colorful stars in an ordered fashion by using a *locked* operation:" << endl;
    for (int i=0; i<numThreads; ++i)
        threads[i] = std::thread(printThreadIdLocked,i+1);
    for (auto& th : threads) th.join(); // join threads again ...
    cout << "\n--> This thime the console output was probably less messy ..." << endl;

    return 0;
}
