/**
 * Example shows the simple usage of std::thread.
 * There is no concurrency in the thread execution
 * because the two threads spend their time in two
 * different functions.
 * Output of the two threads is usually still intermingeld like this:
 * 	  Thread nr: -> main(), firstThread and secondThread now execute concurrently...
 * 	  139727388612352 is working in function "func4Tread1".	Thread nr:
 * 	  139727380219648 is working in function "func4Tread2".
 * 	  firstThread and secondThread completed.
 *
 * @note Dont forget to put "-pthread" into your g++ linker options on linux!
 * @note compile with: g++ -g -Wall -pthread -o simpleThread.exe simpleThread.cpp
 * @see http://www.cplusplus.com/reference/thread/thread/
 * @see http://www.cplusplus.com/reference/thread/thread/get_id/
 * @see https://stackoverflow.com/questions/266168/simple-example-of-threading-in-c
 * */
#include <iostream>       // std::cout
#include <thread>         // std::thread

using namespace std;


void func4Tread1()
{
    cout << "\tThread nr: " <<  std::this_thread::get_id() << " is working in function \"func4Tread1\"." << endl;
}

void func4Tread2()
{
    cout << "\tThread nr: " <<  std::this_thread::get_id() << " is working in function \"func4Tread2\"." << endl;
}

int main()
{
    cout << endl;
    std::thread firstThread(func4Tread1);   // spawn new thread that calls func4Tread1()
    std::thread secondThread(func4Tread2);  // spawn new thread that calls bar(0)

    std::cout << "-> main(), firstThread and secondThread now execute concurrently...\n";

    // synchronize threads:
    firstThread.join();                // pauses until first thread finishes
    secondThread.join();               // pauses until second thread finishes

    std::cout << "firstThread and secondThread completed.\n";

    return 0;
}
