# GDB Debugger

## see: https://www.thegeekstuff.com/2010/03/debug-c-program-using-gdb

## Debugging example:

```
$ gdb gdb.exe 
GNU gdb (Debian 7.12-6) 7.12.0.20161007-git
Copyright (C) 2016 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from gdb.exe...done.

(gdb) help
List of classes of commands:

aliases -- Aliases of other commands
breakpoints -- Making program stop at certain points
data -- Examining data
files -- Specifying and examining files
internals -- Maintenance commands
obscure -- Obscure features
running -- Running the program
stack -- Examining the stack
status -- Status inquiries
support -- Support facilities
tracepoints -- Tracing of program execution without stopping the program
user-defined -- User-defined commands

Type "help" followed by a class name for a list of commands in that class.
Type "help all" for the list of all commands.
Type "help" followed by command name for full documentation.
Type "apropos word" to search for commands related to "word".
Command name abbreviations are allowed if unambiguous.

(gdb) l 41
warning: Source file is more recent than executable.
36	        cerr << "\nUsage example:  \n\t> " << argv[0] << " 2 6 2 5\n"; 
37	        cerr << "\tResult: 15\n" << endl;
38	        return 1;
39	    }
40	    
41	    for (i=1; i<=argc; ++i)
42	        sum+=std::stoi( argv[i] );
43	        
44	    cout << "\nResult: " << sum << endl << endl;
45	    

(gdb) break 41
Breakpoint 1 at 0x10bd: file gdb.cpp, line 41.

(gdb) run 3 5 7
Starting program: ./gdb.exe 3 5 7

argc = 4
arg[0]: ./gdb.exe
arg[1]: 3
arg[2]: 5
arg[3]: 7

Breakpoint 1, main (argc=4, argv=0x7fffffffe1d8) at gdb.cpp:41
41	    for (i=1; i<=argc; ++i)

(gdb) print i
$1 = 1

(gdb) print argv[i]
$2 = 0x7fffffffe4f0 "3"

(gdb) n
40	    

(gdb) p i
$3 = 1

(gdb) n
Breakpoint 1, main (argc=4, argv=0x7fffffffe1d8) at gdb.cpp:41
41	    for (i=1; i<=argc; ++i)

(gdb) p i
$4 = 2

(gdb) p argv[i]
$5 = 0x7fffffffe4f2 "5"

(gdb) n
40	    

(gdb) n
Breakpoint 1, main (argc=4, argv=0x7fffffffe1d8) at gdb.cpp:41
41	    for (i=1; i<=argc; ++i)

(gdb) p i
$6 = 3

(gdb) p argv[i]
$7 = 0x7fffffffe4f4 "7"

(gdb) n
40	    

(gdb) n
Breakpoint 1, main (argc=4, argv=0x7fffffffe1d8) at gdb.cpp:41
41	    for (i=1; i<=argc; ++i)

(gdb) p sum
$8 = 15

(gdb) p i
$9 = 4

(gdb) p argv[i]
$10 = 0x0

(gdb) n
terminate called after throwing an instance of 'std::logic_error'
  what():  basic_string::_M_construct null not valid

Program received signal SIGABRT, Aborted.
__GI_raise (sig=sig@entry=6) at ../sysdeps/unix/sysv/linux/raise.c:51
51	../sysdeps/unix/sysv/linux/raise.c: Datei oder Verzeichnis nicht gefunden.

(gdb) quit
A debugging session is active.

	Inferior 1 [process 20794] will be killed.

Quit anyway? (y or n) yes

> 

```
