////////////////////////////////////////////////////////////////////////////////
// This example shows the usage of the commandline debugger gdb: 
// @Attention:
//              Use the -g switch for compilation with g++ in order 
//              to compile debug symbols into the executable.
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

////////////////////////////////////////////////////////////////////////////////
// _DEBUG: macro controls wether debug output is written to commandline or not.
#define _DEBUG_
//#undef _DEBUG_

using namespace std;

////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    int i=0;
    int sum=0;
    
    // Here the _DEBUG_ macro is usded to give out the contents of argv[]:
    #if defined _DEBUG_
        cout << "\nargc = " << argc << endl;        
        for (i=0; i<argc; ++i) { cout << "arg[" << i << "]: " << argv[i] << endl; }
    #else // you can use this to supress the unused parameter warnings:
        argc = argc;
        argv = argv;
    #endif
    
    if (argc==1 || argv[1]==string("-h")) 
    {
        cerr << "\nProgram calculates the sum from numbers given as parameters at the command line.\n";
        cerr << "\nUsage example:  \n\t> " << argv[0] << " 2 6 2 5\n"; 
        cerr << "\tResult: 15\n" << endl;
        return 1;
    }
    
    for (i=1; i<=argc; ++i)
        sum+=std::stoi( argv[i] );
        
    cout << "\nResult: " << sum << endl << endl;
    
    return 0;
}
