#include <iostream>
#include <string>

////////////////////////////////////////////////////////////////////////////////
// This example shows how to read out the git version of this particular
// commit that can be used in order to track down the exact version of
// an executable for debugging purposes. The git version is read out in the
// makefile and passed as CXXFLAG: -DGIT_VERSION=\"$(GIT_VERSION)\" to the
// compiler.


const char* GITVERSION = GIT_VERSION;

////////////////////////////////////////////////////////////////////////////////
int main()
{
    std::cout << "The git version of this particular build is: " << GITVERSION << std::endl;

    return 0;
}
