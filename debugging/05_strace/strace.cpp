////////////////////////////////////////////////////////////////////////////////
// This example shows the usage of the command line tool strace 
// @Note:
//      This programm tries to open a file that does not exist. 
//      Try to find out which file it tried to open with the help of the 
//      "strace" command line tool and correct the error.
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

////////////////////////////////////////////////////////////////////////////////
// _DEBUG: macro controls wether debug output is written to commandline or not.
//#define _DEBUG_
#undef _DEBUG_

using namespace std;

////////////////////////////////////////////////////////////////////////////////
int main(void)
{
    int i=0;
    int sum=0;
    std::ifstream infile("ABCinput.txt");
    string line;
    string number;
    while (std::getline(infile, line))
    {
        sum=0;
        std::istringstream is(line);
        while ( is >> number ) 
        {
            sum+=std::stoi(number);
            #if defined _DEBUG_
                cerr << "number: " << number << " | sum: " << sum << endl;
            #endif
        }
        
    cout << "Result " << ++i << ". line: " << sum << endl;
    }
        
    return 0;
}
