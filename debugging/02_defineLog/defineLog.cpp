#include <iostream>
#include <string>

////////////////////////////////////////////////////////////////////////////////
// DLOG is a macro that supplies a given log message with the current date and time
// and the file name and line number from where the macro was called. 
#define DLOG( MSG ) ( std::cerr << __DATE__ << ":" << __TIME__ << ": " << __FILE__ << ":" << __LINE__  << ": " << MSG << endl );

////////////////////////////////////////////////////////////////////////////////
int main()
{
    using namespace std;



    DLOG( "Here everything is still alright      :)" )



    DLOG( "Hmmm, not sure what is happening now     :|" )



    DLOG( "Uhhhh! Here happened something awful!!! :(" )
    
    
    return 0;
}
