/**
 * @see https://www.cprogramming.com/tutorial/function-pointers.html
 * @see http://www.newty.de/fpt/fpt.html
 **/
#include <iostream>
using namespace std;

void my_int_func(int x)
{
    cerr << x << endl;
}
 
////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////
int main()
{
    cout << "\nDeclaring a pointer to a function that takes an int as parameter and does not return anything:\n\t\"void (*foo)(int);\"" << endl;
    void (*foo)(int);
    cout << "\nAssigning a function to this function pointer:\n\t\"foo = &my_int_func;\"" << endl;
    foo = &my_int_func;
 
    cout << "\nCalling the function via the function pointer first method:\n\t\"(*foo)( 5 );\"" << endl;
    (*foo)( 5 );
    cout << "\nCalling the function via the function pointer second method:\n\t\"foo( 2 );\"" << endl;
    foo( 2 );
 
    return 0;
}
