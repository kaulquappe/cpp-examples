/**
 * @see https://www.cprogramming.com/tutorial/function-pointers.html
 * @see http://www.newty.de/fpt/fpt.html
 **/
#include <cstdint>
#include <iostream>
#include <vector>

using namespace std;


bool checkEven( const int number )
{
    if (number%2 == 0) return true;
    return false;
}
bool checkUneven( const int number )
{
    if (number%2 != 0)  return true;
    return false;
}
bool checkPositive( const int number )
{
    if (number > 0)  return true;
    return false;
}
bool checkNegative( const int number )
{
    if (number < 0)  return true;
    return false;
}
bool checkZero( const int number )
{
    if (number == 0)  return true;
    return false;
}
bool checkPrime( const int n )
{
   // bool mrt (const uint32_t n, const uint32_t a) { // n ungerade, 1 < a < n-1
   if ( n==1 ) return true;
   if ( n%2 == 0 ) return false;  // even numbers
   
   const uint32_t m = n - 1;
   const uint32_t a = m/2;   
   uint32_t d = m >> 1, e = 1;
   
   while ( !(d & 1) ) d >>= 1, ++e;

   uint64_t p = a, q = a;
   // exponentiere modular: p = a^d mod n
   while( d >>= 1 ) 
   { 
      q *= q, q %= n; // quadriere modular: q = q^2 mod n
      if (d & 1) p *= q, p %= n; // multipliziere modular: p = (p * q) mod n
   }
   if ( p == 1 || p == m ) return true; // n ist wahrscheinlich prim
   while ( --e )
   {
      p *= p, p %= n;
      if ( p == m ) return true;
      if ( p <= 1 ) break;
   }
   return false; // n ist nicht prim
}

struct checkInt
{
    checkInt(bool (*checkNr)(int), const string message) : checkNumber(checkNr), mesg(message) {;}
    bool (*checkNumber)(int);
    const string mesg;
    void printMsg(int number) const
    {
        if ( (*checkNumber)(number) )
            cerr << "Number " << number << " is " << mesg << endl;
        else
            cerr << "Number " << number << " is not " << mesg << endl;
    }
};
 
////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    int number;
    if (argc==1) number = 99991;
    else number=std::stoi(argv[1]);

    vector<checkInt> vecIntChecker;
    vecIntChecker.reserve(8);
    vecIntChecker.emplace_back(&checkPositive, "positive.");
    vecIntChecker.emplace_back(&checkEven, "even.");
    vecIntChecker.emplace_back(&checkUneven, "uneven.");
    vecIntChecker.emplace_back(&checkNegative, "negative.");
    vecIntChecker.emplace_back(&checkZero, "zero.");
    vecIntChecker.emplace_back(&checkPrime, "a prime number.");    

    cout << endl;
    for ( vector<checkInt>::const_iterator itr = vecIntChecker.cbegin(); itr != vecIntChecker.cend(); ++itr )
    {
        itr->printMsg(number);
    }
    cout << endl;
    
    return 0;
}
