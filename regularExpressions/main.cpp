#include <regex>
#include <iostream>

using namespace std;


int main(void)
{
    string str2search01 = "getSetpoint OK: ";
    string str2search02 = "getSetpoint OK: 47";
    string str2search03 = "getSetpoint NOK: ERROR: No value from sensor available!";
    string str2search04 = "This pattern does not match!!";
    
    //regex str_expr("^(getSetpoint )(OK|NOK)(: )([A-Za-z0-9;,:.!?-_+*#+~%=() ]*)$");
    regex str_expr("^(getSetpoint )(OK|NOK)(: )(.*)$");
    
    smatch sm01;
    regex_match( str2search01, sm01, str_expr ); 

    smatch sm02;
    regex_match( str2search02, sm02, str_expr ); 

    smatch sm03;
    regex_match( str2search03, sm03, str_expr ); 

    smatch sm04;
    regex_match( str2search04, sm04, str_expr ); 

    cout << "\n-> \"" << str2search01 << "\":\n\t";
    for (unsigned i=0; i<sm01.size(); ++i) 
    {
        cout << "[" << sm01[i] << "] ";
    }
    cout << endl;
    
    cout << "\n-> \"" << str2search02 << "\":\n\t";
    for (unsigned i=0; i<sm02.size(); ++i) 
    {
        cout << "[" << sm02[i] << "] ";
    }
    cout << endl;

    cout << "\n-> \"" << str2search03 << "\":\n\t";
    for (unsigned i=0; i<sm03.size(); ++i) 
    {
        cout << "[" << sm03[i] << "] ";
    }
    cout << endl;

    cout << "\n-> \"" << str2search04 << "\":\n\t";
    for (unsigned i=0; i<sm04.size(); ++i) 
    {
        cout << "[" << sm04[i] << "] ";
    }
    cout << endl;

    
    return 0;
}
