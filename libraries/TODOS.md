# TODO: Static and Dynamic Libraries

* 01_staticLibrary: done
* 02_sharedLibrary 
* 03_simpleDynamicLoading: done
* 04_advancedDynamicLibrary ( loading classes with strings on windows and linux: see https://theopnv.com/dynamic-loading/ )

## References
---

* https://www.geeksforgeeks.org/static-vs-dynamic-libraries/
* http://www.microhowto.info/howto/build_a_shared_library_using_gcc.html
* https://dwheeler.com/program-library/Program-Library-HOWTO/x172.html
* https://www.tldp.org/HOWTO/html_single/C++-dlopen/
* https://dwheeler.com/program-library/Program-Library-HOWTO/x36.html
* https://dwheeler.com/program-library/Program-Library-HOWTO/x172.html
* http://www.yolinux.com/TUTORIALS/LibraryArchives-StaticAndDynamic.html
* https://theopnv.com/dynamic-loading/
* https://www.linuxjournal.com/article/3687

## Author:
---
* kaulquappe (https://bitbucket.org/kaulquappe/)
