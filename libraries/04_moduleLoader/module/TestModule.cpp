#include <stdio.h>
#include "TestModule.h"

TestModule::TestModule()
{
     printf("Constructor: TestModule::TestModule()\n");
}

TestModule::~TestModule()
{
     printf("Destructor: TestModule::~TestModule()\n");
}

void TestModule::sayHello()
{
    printf("Hello TestModule ;)\n");
}

extern "C" TestModule * getModule()
{
    return new TestModule;
}
