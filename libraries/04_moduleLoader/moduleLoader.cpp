/**
 * Example:
 *  Shows how to dynamically load a symbol(=function: "cos") of an dynamic library (math library).
 *  It shows the usage of dlopen(), dlsym(), dlerror() and dlclose() in C++.
 * Compile with "-ldl" compiler flag in order to use the dynamic link library:
 *  g++ -c -Wall -Wextra -Wpedantic -std=c++11 -g  -o simpleDynamicLoading.o simpleDynamicLoading.cpp
 *  g++  -o simpleDynamicLoading.exe simpleDynamicLoading.o -ldl 
 *
 * @attention Use export LD_LIBRARY_PATH=/path/to/module before issuing ./simpleDynamicLoading.exe !!
 *            The shared object must be in the LD_LIBRARY_PATH or indexed by ldconfig before it can be used !!
 * 
 * @see man 3 dlopen
 * @see https://www.tldp.org/HOWTO/html_single/C++-dlopen/
*/

#include <typeinfo>

#include <iostream>
#include <dlfcn.h>
#include "module/TestModule.h"

using namespace std;

int main(void) 
{
    
    void *handle;    // handle to the dymically linked library we get with dlopen().
    char *error;     // pointer to an error structure

    // here we get a handle to the library:
    // make sure the path to the math library is correct!
    // RTLD_LAZY means that the library symbols are only loaded when needed (in contrast to RTLD_NOW) and not at once.
    // @see man 3 dlopen
    handle = dlopen ("module/libTestModule.so", RTLD_LAZY);
    if (!handle) 
    {
        cerr << "Error in dlopen(): " << dlerror() << endl;
        exit(1);
    }

    typedef TestModule* (*getModuleFnc)(void);
    getModuleFnc getTestModule = reinterpret_cast<TestModule *(*)()>(dlsym( handle, "getModule" ));

    if ( (error = dlerror()) )
    {
        fprintf(stderr, "Couldn't find class TestModule: %s\n", error);
        exit(1);
    }

    /* Now call the function in the DL library */
    TestModule* myTestModule = (*getTestModule)();

    cerr << "Class Type: " << typeid(TestModule).name() << endl;

    myTestModule->sayHello();
    delete myTestModule;

    dlclose(handle);   // releasing the library from memory ...
    return 0;
}
