/**
 * Simple dynamic library with two functions.
 * Compile with:
 * g++ -c -Wall -Wextra -Wpedantic -std=c++11 -g -fPIC  -o myLibrary.o myLibrary.cpp
 * g++  -fPIC  -shared -Wl,-soname,libMyLibrary.so -o libMyLibrary.so.1.0.0 myLibrary.o  
 * ln -s libMyLibrary.so.1.0.0 libMyLibrary.so
 * #cp -vf libMyLibrary.so* /usr/lib/
 * #/sbin/ldconfig
 *
 * @see https://dwheeler.com/program-library/Program-Library-HOWTO/x36.html
 * @see https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html
 */
 
#ifndef __MY_LIBRARY__
#define __MY_LIBRARY__

#include <string>

std::string helloPhilipp();
std::string helloArno();

#endif

