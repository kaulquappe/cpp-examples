/**
 *
 * Compile and link with:
 *  g++ -c -Wall -Wextra -Wpedantic -std=c++11 -g  -o main.o main.cpp
 *  g++ -o staticLib.exe main.o -Llib/ -lMyLibrary
 *
 * @attention -Llib/ is the path where the library should be searched for when linking.
 * @attention -lMyLibrary is the library to be linked with:
 * @attention           -> its without the leading "lib" and the trailing ".so"!
 *
 * Running the programm:
 * > export LD_LIBRARY_PATH=lib/
 * > ./sharedLib.exe 
 *      Arno: Hello Philipp !
 *      Philipp answers: Hello Arno! How are you?
 *
 * @attention you need to tell the linker where your dynamic library is with the "LD_LIBRARY_PATH" environment variable for example!
 * 
 * @see https://dwheeler.com/program-library/Program-Library-HOWTO/x36.html
 * @see https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html
 */ 

#include <iostream>
#include "lib/myLibrary.h"

using namespace std;

int main(void)
{
    cout << "Arno: " << helloPhilipp() << " !" << endl;
    cout << "Philipp answers: " << helloArno() << "! How are you?" << endl;

return 0;
}
