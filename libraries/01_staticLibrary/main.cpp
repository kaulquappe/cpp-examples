/**
 *
 * Compile and link with:
 *  g++ -c -Wall -Wextra -Wpedantic -std=c++11 -g  -o main.o main.cpp
 *  g++ -o staticLib.exe main.o -Llib/ -lMyLibrary
 *
 * @attention -Llib/ is the path where the library should be searched for when linking.
 * @attention -lMyLibrary is the library to be linked with:
 * @attention           -> its without the leading "lib" and the trailing ".a"!
 * @see https://www.geeksforgeeks.org/static-vs-dynamic-libraries/
 */ 

#include <iostream>
#include "lib/myLibrary.h"

using namespace std;

int main(void)
{
    cout << "Arno: " << helloPhilipp() << " !" << endl;
    cout << "Philipp answers: " << helloArno() << "! How are you?" << endl;

return 0;
}
