/**
 * Simple static library with two functions.
 * Compile with:
 *  g++ -c -Wall -Wextra -Wpedantic -std=c++11 -g  -o myLibrary.o myLibrary.cpp
 *  ar crv libMyLibrary.a myLibrary.o
 * ranlib libMyLibrary.a
 *
 * @see https://www.geeksforgeeks.org/static-vs-dynamic-libraries/
 */

#ifndef __MY_LIBRARY__
#define __MY_LIBRARY__

#include <string>

std::string helloPhilipp();
std::string helloArno();

#endif

