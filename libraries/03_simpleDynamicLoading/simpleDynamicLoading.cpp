/**
 * Example:
 *  Shows how to dynamically load a symbol(=function: "cos") of an dynamic library (math library).
 *  It shows the usage of dlopen(), dlsym(), dlerror() and dlclose() in C++.
 * Compile with "-ldl" compiler flag in order to use the dynamic link library:
 *  g++ -c -Wall -Wextra -Wpedantic -std=c++11 -g  -o simpleDynamicLoading.o simpleDynamicLoading.cpp
 *  g++  -o simpleDynamicLoading.exe simpleDynamicLoading.o  -ldl 
 * 
 * @see man 3 dlopen
 * @see https://www.tldp.org/HOWTO/html_single/C++-dlopen/
*/
#include <iostream>
#include <dlfcn.h>

using namespace std;

int main(void) 
{
    
    void *handle;               // handle to the dymically linked library we get with dlopen().
    double (*cosine)(double);   // a pointer to a function that is called "cosine" returns a double and gets a doule as parameter.
    char *error;                // pointer to an error structure?

    // here we get a handle to the library:
    // make sure the path to the math library is correct!
    // RTLD_LAZY means that the library symbols are only loaded when needed (in contrast to RTLD_NOW) and not at once.
    // @see man 3 dlopen
    handle = dlopen ("/lib/x86_64-linux-gnu/libm.so.6", RTLD_LAZY);
    if (!handle) 
    {
        cerr << "Error in dlopen(): " << dlerror() << endl;
        exit(1);
    }

    // here we need to reinterpret cast the return value of dlysm() because the 
    // type cannot be checked by the compiler at compile time
    cosine = reinterpret_cast<double (*)(double)>(dlsym(handle, "cos"));
    if ((error = dlerror()) != NULL)  
    {
        cerr << "Error in dlsym(): " << dlerror() << endl;
        exit(1);
    }

    cout << "Cosinus of 2.00: " << (*cosine)(2.0) << endl; 
    dlclose(handle);   // releasing the library from memory ...
    return 0;
}
