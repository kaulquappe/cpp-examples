#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <list>
using namespace std;

class MyClass
{
    const int val1_;
    const string val2_;
public:
    explicit MyClass(int i = 0, string j = "") : val1_(i), val2_(j)
    {
        cerr << "Constructor this pointer: " << this << " of key: " << getVal2() << endl;
    }
    MyClass(const MyClass &c) : val1_(c.val1_), val2_(c.val2_)
    {
        cerr << "CopyConstructor this pointer: " << this << " of key: " << getVal2()
             << " / myClass &c: " << &c << endl;
    }
    const int getVal1() const { return val1_; }
    const string getVal2() const { return val2_; }
};

int main()
{
    /**
     * @see http://www.cplusplus.com/reference/stl/
     * @see https://www.geeksforgeeks.org/the-c-standard-template-library-stl/
    */
    cout << "\nContainers of the \"Standard Template Library\":";
////////////////////////////////////////////////////////////////////////////////
    /**
     * @see http://www.cplusplus.com/reference/vector/vector/
     * @see https://www.geeksforgeeks.org/vector-in-cpp-stl/
    */
    cout << "\n\n\t-> std::vector:" << endl;

    vector<int> vecInt;
    for (int i = 1; i <= 5; ++i)
        vecInt.push_back(i);

    // const_iterator is faster than iterator but elements cannot be modified.
    // const_iterator also shows the type when compared to auto keyword in next example.
    cout << "\t\tpushed back 5 numbers on vecInt: ";
    for (vector<int>::const_iterator it = vecInt.begin(); it != vecInt.end(); ++it)
        {
            cout << *it << " ";
        }
    cout << endl;

    vecInt[1] = 0;
    vecInt.insert(vecInt.begin()+3, 11);
    vecInt.at(4) = 77;

    // note the use of the auto keyword instead the const_iterator keyword!
    cout << "\t\tnow reversing vecInt: ";
    for (auto ir = vecInt.crbegin(); ir != vecInt.crend(); ++ir)
        {
            cout << *ir << " ";
        }
    cout << endl;

    cout << "\t\tvecInt is " << (vecInt.empty() ? "":"not ") << "empty!" << endl;
    cout << "\t\tfirst element: " << vecInt.front() << endl;
    cout << "\t\tlast element: " << vecInt.back() << endl;
    cout << "\t\tsize of vecInt: " << vecInt.size() << endl;
    cout << "\t\tcapacity of vecInt: " << vecInt.capacity() << endl;
    cout << "\t\tmax_size of vecInt: " << vecInt.max_size() << endl;

    // Question: Why does the code below result in a segmentation fault ???
    // Answer: ...
    //~ cout << "\t\tdeleting: ";
    //~ for (vector<int>::iterator it = vecInt.begin(); it != vecInt.end(); ++it)
        //~ {
            //~ cout << *it << " ";
            //~ vecInt.erase (it);
        //~ }
    //~ cout << endl;

    cout << "\t\tdeleting: ";
    vector<int>::iterator itV = vecInt.begin();
    while (itV != vecInt.end())
        {
            cout << *itV << " ";
            vecInt.erase(itV);
            itV = vecInt.begin();
        }
    cout << endl;

////////////////////////////////////////////////////////////////////////////////
    /**
     * std::map is an associative container that consists of key->value *pairs*.
     * the value can be looked up by searching for a key.
     * @see http://www.cplusplus.com/reference/map/map/
     * @see https://www.geeksforgeeks.org/map-associative-containers-the-c-standard-template-library-stl/
     *
    */
    cout << "\n\t-> std::map:" << endl;

    map<int, string> mapStr;
    mapStr.insert(pair<int, string>(1, "oans"));
    mapStr.insert(pair<int, string>(2, "zwoa"));
    mapStr.insert(pair<int, string>(3, "drai"));
    mapStr[4] = "viare";
    mapStr[5] = "finfe";
    mapStr[100] = "hundat";
    mapStr[50] = "fünfzig";

    cout << "\t\tcounting the tyrolian way: \n";
    for (map<int, string>::const_iterator it = mapStr.begin(); it != mapStr.end(); ++it)
    {
        cout << "\t\t\t" << it->first << ": \t" << it->second << '\n';
    }
    cout << endl;

    cout << "\t\tremoved 3rd element: \n";
    mapStr.erase(3);
    for (map<int, string>::const_iterator it = mapStr.begin(); it != mapStr.end(); ++it)
    {
        cout << "\t\t\t" << it->first << ": \t" << it->second << '\n';
    }
    cout << endl;

    cout << "\t\taccessing nonexisting values:" << endl;
    string strValue = mapStr[55];
    if (strValue.empty()) cerr << "\t\t\tError: Value 55 does not exist!\n";
    if (mapStr.count(66)) { cout << "\t\t\t66: " << mapStr[66] << endl; }
    else cerr << "\t\t\tError: Value 66 does not exist!\n";
    map<int, string>::const_iterator itF = mapStr.find(77);
    if ( itF != mapStr.end() ) { cout << "\t\t\t77: " << itF->second << endl; }
    else cerr << "\t\t\tError: Value 77 does not exist!\n";

    try
    {
        cout << "\t\t\t5 : " << mapStr.at(5) << endl;
        cout << "\t\t\t55: " << mapStr.at(55) << endl;
        cout << "\t\t\t22: " << mapStr.at(22) << endl;
    }
    catch(std::out_of_range &ex)
    {
        cerr << "\t\t\tError: std::out_of_range in: " << ex.what() << endl;
    }
    cout << endl;


    map<string, int> mapInt;
    mapInt["eins"] = 1;
    mapInt["zwei"] = 2;
    mapInt["acht"] = 8;
    mapInt["zehn"] = 10;
    mapInt["fünfzehn"] = 15;
    mapInt["hundert"] = 100;
    mapInt["tausend"] = 1000;

    cout << "\t\tcounting the german way: \n";
    for (map<string, int>::const_iterator it = mapInt.begin(); it != mapInt.end(); ++it)
    {
        cout << "\t\t\t" << it->first << ": \t" << it->second << '\n';
    }

////////////////////////////////////////////////////////////////////////////////
    /**
     * Lists are sequence containers that allow constant time insert and erase operations
     * anywhere within the sequence, and iteration in both directions.
     * @see http://www.cplusplus.com/reference/list/list/
     * @see https://www.geeksforgeeks.org/list-cpp-stl/
    */
    cout << "\n\n\t-> std::list:" << endl;

    list<int> listInt, listInt2;
    for (int i = 1; i <= 5; ++i)
        listInt.push_back(i);

    // const_iterator is faster than iterator but elements cannot be modified.
    // const_iterator also shows the type when compared to auto keyword in next example.
    cout << "\t\tpushed back 5 numbers on listInt: ";
    for (list<int>::const_iterator it = listInt.begin(); it != listInt.end(); ++it)
        {
            cout << *it << " ";
        }
    cout << endl;

    listInt.insert(listInt.begin()/*+3*/, 11);              // iterator can be addressed with increment and decrement but not with operator+
    listInt2.assign(listInt.begin(), --listInt.end());

    // note the use of the auto keyword instead the const_iterator keyword!

    cout << "\t\tassigned all but the last element of listInt to listInt2: ";
    for (auto i = listInt2.cbegin(); i != listInt2.cend(); ++i)
        {
            cout << *i << " ";
        }
    cout << endl;

    cout << "\t\tnow reversing listInt: ";
    for (auto ir = listInt.crbegin(); ir != listInt.crend(); ++ir)
        {
            cout << *ir << " ";
        }
    cout << endl;

    cout << "\t\tlistInt is " << (listInt.empty() ? "":"not ") << "empty!" << endl;
    cout << "\t\tfirst element: " << listInt.front() << endl;
    cout << "\t\tlast element: " << listInt.back() << endl;
    cout << "\t\tsize of listInt: " << listInt.size() << endl;
    cout << "\t\tmax_size of listInt: " << listInt.max_size() << endl;

    cout << "\t\tdeleting: ";
    list<int>::iterator itL = listInt.begin();
    while (itL != listInt.end())
        {
            cout << *itL << " ";
            listInt.erase(itL);
            itL = listInt.begin();
        }
    cout << endl;

   /**
     * difference between list::emplace() and list::insert()
     * @see https://www.qtcentre.org/threads/68896-What-s-the-difference-between-emplace-amp-insert-in-std-vector-or-in-std-list
     */

    cout << "\nConstructors: " << endl;
    MyClass m(99, "insert");
    list<MyClass> l;
    l.emplace(l.end(), 23, "emplace");
    l.insert(l.end(), m);
    l.emplace(l.end(), 23, "emplace");           // in this case insert() and emplace() are not interchangeable

    //l.insert(l.end(), 99);     compiler error with explicit constructor: "insert() takes a reference to an already-constructed instance"  --- But I have passed a unconstructed element.
    //l.emplace(l.end(), m);     "emplace() constructs a new instance in place"   --- I have passed a already constructed element.

    cout << "\n\tlist::insert() vs. list::emplace(): " << endl;
    list<MyClass>::const_iterator citL;
    for (citL = l.cbegin(); citL != l.cend(); ++citL)               // (auto i : l) uses CopyConstructor
        cout << "\t\t (" << citL->getVal1() << ", " << citL->getVal2() << ") " << endl;
    cout << endl;

    for (citL = l.cbegin(); citL != l.cend(); ++citL)
        {
            cout << "\tAddress of object with key '" << citL->getVal2() << "’: " << &(*citL) << endl;
        }

////////////////////////////////////////////////////////////////////////////////
    /**
     * @todo set, deque, queue, priority_queue, stack, multimap, multiset, forward_list
     */


    cout << endl;
    return 0;
}
