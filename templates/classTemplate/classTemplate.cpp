#define BUFFERSIZE 12
#include <iostream>

using namespace std;

template <typename T>
class RingBuffer
{
    public:
        RingBuffer() : curPos_(0)
        {}

        void write(T msg[], size_t count)
        {
            //size_t count = sizeof(msg)/sizeof(*msg);      kann nur im main berechnet werden nicht für Übergabeparameter msg[]
            for (size_t i = 0; i < count; ++i)
            {
                ringBuffer_[curPos_] = msg[i];
                ++curPos_;
                curPos_ = curPos_%BUFFERSIZE;
                //curPos_ = ++curPos_%BUFFERSIZE;           ergibt Warning, dass curPos_ undefiniert sein könnte
            }
        }
        void print() const
        {
            cout << "curPos_ = " << curPos_ << endl;
            for (int i = 0; i < BUFFERSIZE; ++i)
            {
                cout << ringBuffer_[(i+curPos_)%BUFFERSIZE] << endl;
            }
        };
    private:
        T ringBuffer_[BUFFERSIZE] = {0};
        size_t curPos_;
};

int main()
{
    RingBuffer<char> myRingBuffer;

    char arr1[] = {'H','e','l','l','o',' '};

    myRingBuffer.write(arr1, sizeof(arr1)/sizeof(*arr1));
    myRingBuffer.print();

    char arr2[] = {'W','o','r','l','d',' ','H','e','l','l','o',' '};

    myRingBuffer.write(arr2, sizeof(arr2)/sizeof(*arr2));
    myRingBuffer.print();

    return 0;
}
