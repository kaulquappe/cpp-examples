/*
 * @note that push_back always copies objects in memory ( in comparison to emplace ) !!
*/

#include <iostream>
#include <vector>

using namespace std;

class myClass
{
public:
    explicit myClass() : id_(myClass::getCounter())
    {
        cout << id_ << ": Constructor this pointer: " << this << endl;        
    }

    explicit myClass(const myClass &c) : id_(c.id_)
    {
        cout << id_ << ": CopyConstructor this pointer: " << this
             << " / myClass &c: " << &c << endl;
    }
    
    void print() const
    {
        cout << id_ << ": Print this pointer: " << this << endl;
    }

private:
    static int getCounter();
    static int counter;
    int id_;
};

int myClass::counter = 0;
int myClass::getCounter() {return myClass::counter++;}

/////////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////////
int main( void )
{
    cout << endl;

    std::vector<myClass> myVec;
    // @attention: reserve enough space in the vector before you fill it because
    //             otherwise a lot of memory reserving and copying is done!!!
    myVec.reserve(32);
    myVec.push_back( myClass() );
    myVec.push_back( myClass() );
    myVec.push_back( myClass() );
    
    cout << endl;    
    for ( vector<myClass>::const_iterator it = myVec.cbegin(); it != myVec.cend(); ++it )
    {
        it->print();
    }
    
    cout << endl;
    cout << "myVec[0]: " << &(myVec[0]) << endl;
    cout << "myVec[1]: " << &(myVec[1]) << endl;
    cout << "myVec[2]: " << &(myVec[2]) << endl;
    cout << endl;
    cout << "Printing\n";
    myVec[0].print();
    myVec[1].print();
    myVec[2].print();
    cout << endl;
 
    cout << "size of myVec: " << myVec.size() << endl << endl;
}
