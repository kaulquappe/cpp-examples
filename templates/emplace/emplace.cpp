/*
 * @note emplace does only copy objects when an object is given as parameter
 *       it then behaves like push_back. Otherwise it does not make unnecessary copies !!
*/

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class myClass
{
public:
    explicit myClass(const int id, const string name) : id_(id), name_(name)
    {
        cerr << id_ << ": " << name_ << ": Constructor this pointer: " << this << endl;        
    }

    explicit myClass(const myClass &c) : id_(c.id_), name_(c.name_)
    {
        cerr << id_ << ": " << name_ << ": CopyConstructor this pointer: " << this
             << " / myClass &c: " << &c << endl;        
    }

    void print() const
    {
        cerr << id_ << ": " << name_ << ": Print this pointer: " << this << endl;
    }

private:
    const int id_;
    const string name_;
};

/////////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////////
int main( void )
{
    cerr << endl;

    std::vector<myClass> myVec;
    // @attention: reserve enough space in the vector before you fill it because
    //             otherwise a lot of memory reserving and copying is done!!!
    myVec.reserve(32);
    myVec.emplace_back( 0, "nix" );
    myVec.emplace_back( 1, "oans" );
    myVec.emplace_back( 2, "zwoa" );
    
    cerr << endl;    
    for ( vector<myClass>::const_iterator it = myVec.cbegin(); it != myVec.cend(); ++it )
    {
        it->print();
    }
    
    cerr << endl;
    cerr << "myVec[0]: " << &(myVec[0]) << endl;
    cerr << "myVec[1]: " << &(myVec[1]) << endl;
    cerr << "myVec[2]: " << &(myVec[2]) << endl;
    cerr << endl;
    cerr << "Printing\n";
    myVec[0].print();
    myVec[1].print();
    myVec[2].print();
    cerr << endl;
    cerr << "size of myVec: " << myVec.size() << endl << endl;
}
