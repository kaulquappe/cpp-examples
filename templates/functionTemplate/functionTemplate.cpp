//function template
#include <iostream>
using namespace std;

template <typename T>
T add(T a, T b)
{
    return a + b;
}

int main()
{
    int isum = add<int>(7,5);
    cout << "isum: " << isum << endl;

    string strsum = add<string>("Hello ","World!");
    cout << "strsum: " << strsum << endl;

    double dsum = add<double>(3.333,6.666);
    cout << "dsum: " << dsum << endl;
    
    return 0;
}
